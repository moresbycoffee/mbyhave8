/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit.utils;

import java.util.HashMap;
import java.util.Map;

import lombok.val;

/**
 * <p>PropertySetter can set/replace {@link System#getProperties() System properties}
 * until the {@link #close()} method is invoked. The {@link #close()} method
 * reverts all the changes.</p>
 *
 * <p>PropertySetter implements AutoCloseable interface, therefore it can be used in
 * <code>try(...) {}</code> blocks.</p>
 *
 * <strong>Example:</strong>
 * <code><pre>
 * try(PropertySetter ps = PropertySetter.setSystemProperties("property.key", "property.value")) {
 *     ...
 *     //test code
 *     ...
 * }
 * </pre></code>
 */
public class PropertySetter implements AutoCloseable {

    public static PropertySetter setSystemProperties(final Map<String, String> propertiesToSet) {
        return new PropertySetter(propertiesToSet);
    }
    public static PropertySetter setSystemProperties(final String... strings) {
        if (strings == null || strings.length % 2 != 0) {
            throw new IllegalArgumentException("This method takes even number of parameters. (key, value pairs)");
        }
        final Map<String, String> propertiesToSet = new HashMap<>();
        for (int i =0; i + 1 < strings.length; i = i+2) {
            propertiesToSet.put(strings[i], strings[i+1]);
        }
        return setSystemProperties(propertiesToSet);
    }

    private final Map<String, String> originalProperties = new HashMap<>();

    public PropertySetter(final Map<String, String> propertiesToSet) {
        for (val propertyToSet : propertiesToSet.entrySet()) {
            val originalValue = System.getProperty(propertyToSet.getKey());
            originalProperties.put(propertyToSet.getKey(), originalValue == null ? "" : originalValue);
            System.setProperty(propertyToSet.getKey(), propertyToSet.getValue());
        }
    }

    @Override
    public void close() {
        for (val originalProperty : originalProperties.entrySet()) {
            System.setProperty(originalProperty.getKey(), originalProperty.getValue());
        }
    }

}