/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit;

import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.runner.Description;
import org.moresbycoffee.mbyhave8.*;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;
import org.moresbycoffee.mbyhave8.structure.Specification;
import org.moresbycoffee.mbyhave8.structure.Step;

/**
 * Created with IntelliJ IDEA.
 * User: envagyok
 * Date: 30/11/13
 * Time: 18:22
 * To change this template use File | Settings | File Templates.
 */
public class TestUtils {

    public static Step failingGivenStep() {
        return new MByHaveSpec() {
            Step innerStep = given("Failing Given Step", (VoidStepImplementation) () -> { throw new AssertionError(); });
        }.innerStep;
    }

    public static Step pendingGivenStep() {
        return new MByHaveSpec() {
            Step innerStep = given("Pending Given Step", (VoidStepImplementation) () -> { throw new PendingException(); });
        }.innerStep;
    }

    public static Step errorGivenStep() {
        return new MByHaveSpec() {
            Step innerStep = given("Erroneous Given Step ", (VoidStepImplementation) () -> { throw new RuntimeException(); });
        }.innerStep;
    }

    public static Step successGivenStep() {
        return new MByHaveSpec() {
            Step innerStep = given("Successful Given Step", () -> { return; });
        }.innerStep;
    }

    public static Step failingWhenStep() {
        return new MByHaveSpec() {
            Step innerStep = when("Failing When Step", (VoidStepImplementation) () -> {
                throw new AssertionError();
            });
        }.innerStep;
    }

    public static Step pendingWhenStep() {
        return new MByHaveSpec() {
            Step innerStep = when("Pending When Step", (VoidStepImplementation) () -> {
                throw new PendingException();
            });
        }.innerStep;
    }

    public static Step errorWhenStep() {
        return new MByHaveSpec() {
            Step innerStep = when("Test description", (VoidStepImplementation) () -> {
                throw new RuntimeException();
            });
        }.innerStep;
    }

    public static Step successWhenStep() {
        return new MByHaveSpec() {
            Step innerStep = when("Test description", () -> {
                return;
            });
        }.innerStep;
    }



    public static Step failingThenStep() {
        return new MByHaveSpec() {
            Step innerStep = then("Test description", (VoidStepImplementation) () -> {
                throw new AssertionError();
            });
        }.innerStep;
    }

    public static Step pendingThenStep() {
        return new MByHaveSpec() {
            Step innerStep = then("Test description", (VoidStepImplementation) () -> {
                throw new PendingException();
            });
        }.innerStep;
    }

    public static Step errorThenStep() {
        return new MByHaveSpec() {
            Step innerStep = then("Test description", (VoidStepImplementation) () -> {
                throw new RuntimeException();
            });
        }.innerStep;
    }

    public static Step successThenStep() {
        return new MByHaveSpec() {
            Step innerStep = then("Test description", () -> {
                return;
            });
        }.innerStep;
    }

    public static Scenario scenarioWith(Step... steps) {
        return new MByHaveSpec() {
            final Scenario scenario = Scenario("Scenario description", steps);
        }.scenario;
    }



    public static Feature featureWith(final Scenario... scenarios) {
        return featureWith("Feature description", scenarios);
    }

    public static Feature featureWith(final String name, final Scenario... scenarios) {
        return new Feature(name, scenarios);
    }

    public static Specification specificationWith(Feature... features) {
        return new Specification(features);
    }

    public static Scenario successScenario() {
        return scenarioWith(successGivenStep());
    }

    public static Scenario pendingScenario() {
        return scenarioWith(pendingGivenStep());
    }

    public static Scenario failingScenario() {
        return scenarioWith(failingGivenStep());
    }

    public static Scenario errorScenario() {
        return scenarioWith(errorGivenStep());
    }

    public static Matcher<Description> descriptionStartingWith(String description) {
        return new TypeSafeMatcher<Description>() {

            private Boolean result = null;
            private String testDescription = null;
            @Override
            protected boolean matchesSafely(final Description descriptionObj) {
                boolean res = descriptionObj != null && descriptionObj.getDisplayName().startsWith(description);
                result = Boolean.valueOf(res);
                testDescription = descriptionObj == null ? null : descriptionObj.getDisplayName();
                return res;
            }

            @Override
            public void describeTo(final org.hamcrest.Description hamcrestDesc) {
                hamcrestDesc.appendText("a description with \"" + description + "\"");
                if (result != null && !result) {
                    hamcrestDesc.appendText(" but it was: " + testDescription);
                }
            }
        };
    }
}
