/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit.filters;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;
import org.moresbycoffee.mbyhave8.filter.Filter;

/**
 * Unit tests of {@link Filter#isExecutable(String)} method.
 */
public class Filter_isExecutableTest {

    @Test
    public void empty_filter_should_return_true_to_any_tags() {
        final Filter emptyFilter = new Filter(null);

        assertTrue(emptyFilter.isExecutable(null));
        assertTrue(emptyFilter.isExecutable(Collections.emptyList()));
        assertTrue(emptyFilter.isExecutable(Arrays.asList("tag1", "tag2")));
    }

    @Test
    public void a_single_tag_filter_should_return_true_if_input_contain_that_tag() {
        final Filter singleTagFilter = new Filter("tag1");

        assertTrue(singleTagFilter.isExecutable(Arrays.asList("tag1")));
        assertTrue(singleTagFilter.isExecutable(Arrays.asList("tag0", "tag1", "tag2")));
        assertFalse(singleTagFilter.isExecutable(null));
        assertFalse(singleTagFilter.isExecutable(Arrays.asList("tag2")));
        assertFalse(singleTagFilter.isExecutable(Arrays.asList("tag3", "tag4")));
    }

    @Test
    public void a_single_exclued_tag_filter_should_return_true_if_input_does_not_contain_that_tag() {
        final Filter singleExclusion = new Filter("~tag1");
        assertFalse(singleExclusion.isExecutable(Arrays.asList("tag1")));
        assertTrue(singleExclusion.isExecutable(null));
        assertTrue(singleExclusion.isExecutable(Collections.emptyList()));
        assertTrue(singleExclusion.isExecutable(Arrays.asList("tag2")));
        assertFalse(singleExclusion.isExecutable(Arrays.asList("tag2", "tag1")));
    }

    @Test
    public void two_include_tag() {
        final Filter doubleInclusion = new Filter("tag1, tag2");
        assertFalse(doubleInclusion.isExecutable(null));
        assertFalse(doubleInclusion.isExecutable(Collections.emptyList()));
        assertTrue(doubleInclusion.isExecutable(Arrays.asList("tag1", "tag3")));
        assertTrue(doubleInclusion.isExecutable(Arrays.asList("tag2", "tag4")));
    }

    @Test
    public void a_include_and_an_exclude_tag() {
        final Filter doubleInclusion = new Filter("tag1, ~tag2");
        assertFalse(doubleInclusion.isExecutable(null));
        assertFalse(doubleInclusion.isExecutable(Collections.emptyList()));
        assertTrue(doubleInclusion.isExecutable(Arrays.asList("tag1", "tag3")));
        assertFalse(doubleInclusion.isExecutable(Arrays.asList("tag2", "tag4")));
        assertFalse(doubleInclusion.isExecutable(Arrays.asList("tag1", "tag2")));
        assertTrue(doubleInclusion.isExecutable(Arrays.asList("tag4", "tag1")));
    }

}
