/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit.filters;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.moresbycoffee.mbyhave8.filter.Filter;
import org.moresbycoffee.mbyhave8.filter.FilterItem.Exclusion;
import org.moresbycoffee.mbyhave8.filter.FilterItem.Inclusion;

public class Filter_initTest {

    @Test
    public void should_be_configured_empty_when_input_is_null() {
        assertTrue("The filter should be emtpy", new Filter(null).getFilterTags().length == 0);
    }

    @Test
    public void should_be_configured_empty_when_input_contains_only_white_spaces() {
        assertTrue("The filter should be emtpy", new Filter("    ").getFilterTags().length == 0);
    }

    @Test
    public void should_be_configured_with_the_single_tag_fed_into_constructor() {
        final Filter filter = new Filter("tag1");
        assertEquals(1, filter.getFilterTags().length);
        assertEquals(new Inclusion("tag1"), filter.getFilterTags()[0]);
    }

    @Test
    public void should_be_configured_with_the_coma_separated_tags_fed_into_constructor() {
        final Filter filter = new Filter("tag1,tag2");
        assertThat(filter.getFilterTags(), arrayContaining(new Inclusion("tag1"), new Inclusion("tag2")));
    }

    @Test
    public void should_be_configured_with_trimmed_coma_separated_tags_fed_into_constructor() {
        final Filter filter = new Filter("    tag1 , tag2,tag3,~tag4    ");
        assertThat(filter.getFilterTags(), arrayContaining(new Inclusion("tag1"), new Inclusion("tag2"), new Inclusion("tag3"), new Exclusion("~tag4")));
    }

}
