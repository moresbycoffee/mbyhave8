/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit.annotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Test;
import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.acceptance.HookTestSpec;
import org.moresbycoffee.mbyhave8.annotations.BeforeScenario;
import org.moresbycoffee.mbyhave8.annotations.HookAnnotations;
import org.moresbycoffee.mbyhave8.error.LifeCycleHookException;
import org.moresbycoffee.mbyhave8.hooks.MByHave8Hooks;
import org.moresbycoffee.mbyhave8.result.FeatureResult;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;
import org.moresbycoffee.mbyhave8.structure.Specification;

public class HookAnnotations_toHookTest {

    @Test
    public void before_spec_method_should_be_converted_to_hook() throws Throwable {
        HookTestSpec.beforeSpec.set(new AtomicBoolean(false));

        final HookTestSpec hookTestSpec = new HookTestSpec();
        final Method[] beforeSpecMethods = new Method[] { HookTestSpec.class.getDeclaredMethod("beforeSpecification", Specification.class) };
        final MByHave8Hooks hook = new HookAnnotations(beforeSpecMethods, null, null, null, null, null).toMByHaveHook(hookTestSpec);

        hook.startSpecification(mock(Specification.class));

        assertTrue(HookTestSpec.beforeSpec.get().get());

    }

    @Test
    public void after_spec_method_should_be_converted_to_hook() throws Exception {
        HookTestSpec.afterSpec.set(new AtomicBoolean(false));

        final HookTestSpec hookTestSpec = new HookTestSpec();
        final Method[] afterSpecMethods = new Method[] { HookTestSpec.class.getDeclaredMethod("afterSpecification", Specification.class, SpecOutput.class) };
        final MByHave8Hooks hook = new HookAnnotations(null, afterSpecMethods, null, null, null, null).toMByHaveHook(hookTestSpec);

        hook.endSpecification(mock(Specification.class), mock(SpecOutput.class));

        assertTrue(HookTestSpec.afterSpec.get().get());

    }

    @Test
    public void before_feature_method_should_be_converted_to_hook() throws Exception {
        HookTestSpec.beforeFeature.set(new AtomicBoolean(false));

        final HookTestSpec hookTestSpec = new HookTestSpec();
        final Method[] beforeFeatureMethods = new Method[] { HookTestSpec.class.getDeclaredMethod("beforeFeature", Feature.class) };
        final MByHave8Hooks hook = new HookAnnotations(null, null, beforeFeatureMethods, null, null, null).toMByHaveHook(hookTestSpec);

        hook.startFeature(mock(Feature.class));

        assertTrue(HookTestSpec.beforeFeature.get().get());

    }

    @Test
    public void after_feature_method_should_be_converted_to_hook() throws Exception {
        HookTestSpec.afterFeature.set(new AtomicBoolean(false));

        final HookTestSpec hookTestSpec = new HookTestSpec();
        final Method[] afterFeatureMethods = new Method[] { HookTestSpec.class.getDeclaredMethod("afterFeature", Feature.class, FeatureResult.class) };
        final MByHave8Hooks hook = new HookAnnotations(null, null, null, afterFeatureMethods, null, null).toMByHaveHook(hookTestSpec);

        hook.endFeature(mock(Feature.class), FeatureResult.Success);

        assertTrue(HookTestSpec.afterFeature.get().get());

    }

    @Test
    public void before_scenario_method_should_be_converted_to_hook() throws Exception {
        HookTestSpec.beforeScenario.set(new AtomicBoolean(false));

        final HookTestSpec hookTestSpec = new HookTestSpec();
        final Method[] beforeScenarioMethods = new Method[] { HookTestSpec.class.getDeclaredMethod("beforeScenario", Scenario.class) };
        final MByHave8Hooks hook = new HookAnnotations(null, null, null, null, beforeScenarioMethods, null).toMByHaveHook(hookTestSpec);

        hook.startScenario("TEST_FEATURE_ID", mock(Scenario.class));

        assertTrue(HookTestSpec.beforeScenario.get().get());

    }

    @Test
    public void after_scenario_method_should_be_converted_to_hook() throws Exception {
        HookTestSpec.afterScenario.set(new AtomicBoolean(false));

        final HookTestSpec hookTestSpec = new HookTestSpec();
        final Method[] afterScenarioMethods = new Method[] { HookTestSpec.class.getDeclaredMethod("afterScenario", Scenario.class, ScenarioResult.class) };
        final MByHave8Hooks hook = new HookAnnotations(null, null, null, null, null, afterScenarioMethods).toMByHaveHook(hookTestSpec);

        hook.endScenario("TEST_FEATURE_ID", mock(Scenario.class), ScenarioResult.Success);

        assertTrue(HookTestSpec.afterScenario.get().get());

    }

    @Test(expected = LifeCycleHookException.class)
    public void should_throw_exception_if_error_happens_in_hook_execution() throws Exception {

        final Method[] beforeScenarioMethods = new Method[] { HookExceptionTestSpec.class.getDeclaredMethod("beforeScenario") };
        final MByHave8Hooks hook = new HookAnnotations(null, null, null, null, beforeScenarioMethods, null).toMByHaveHook(new HookExceptionTestSpec());

        hook.startScenario("TEST_FEATURE_ID", mock(Scenario.class));
    }

    public static class HookExceptionTestSpec extends MByHaveSpec {
        {
            Feature("test feature",
                Scenario("test scenario"));
        }

        @BeforeScenario
        public void beforeScenario() {
            throw new RuntimeException();
        }
    }



}
