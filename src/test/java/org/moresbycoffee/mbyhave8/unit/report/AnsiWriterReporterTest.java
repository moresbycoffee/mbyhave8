/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit.report;

import static java.util.Arrays.*;
import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;
import static org.junit.Assert.*;
import static org.moresbycoffee.mbyhave8.unit.utils.PropertySetter.*;

import java.io.StringWriter;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.report.AnsiWriterReporter;
import org.moresbycoffee.mbyhave8.report.Reporter;
import org.moresbycoffee.mbyhave8.result.FeatureOutput;
import org.moresbycoffee.mbyhave8.result.FeatureResult;
import org.moresbycoffee.mbyhave8.result.ScenarioOutput;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.result.SpecResult;
import org.moresbycoffee.mbyhave8.result.StepOutput;
import org.moresbycoffee.mbyhave8.result.StepResult;
import org.moresbycoffee.mbyhave8.utils.ThrowableExtensions;

import lombok.val;
import lombok.experimental.ExtensionMethod;

/**
 * Unit tests for {@link AnsiWriterReporter}.
 */
@ExtensionMethod({ThrowableExtensions.class})
public class AnsiWriterReporterTest {

    @Test
    public void should_report_a_successful_feature_in_all_green() {
        final MByHaveSpec spec = new MByHaveSpec() {{
            Feature("this is a feature without runner",
                    Scenario("this is a scenario",
                            given("something", () -> {})));
        }};
        final SpecOutput output = spec.execute();


        final String reportOutput = report(output);

        assertEquals(ansi().fg(GREEN).a("Feature: this is a feature without runner").reset().newline().
                            fg(GREEN).a("    Scenario: this is a scenario").reset().newline().
                            fg(GREEN).a("        Given something").reset().newline().toString(),
                reportOutput);

    }

    @Test
    public void report_should_print_out_the_exception_from_a_failing_step() {

        final Throwable error = new RuntimeException();

        final String reportOutput = reportOutputWith(error);

        final String stackTrace = error.convertToString();


        assertEquals(ansi().fg(RED).a("Feature: Feature in error").reset().newline().
                fg(RED).a("    Scenario: Scenario in error").reset().newline().
                fg(RED).a("        Given an error step").reset().newline().
                a(stackTrace).toString(),
                reportOutput);

    }

    @Test
    public void report_should_print_out_the_first_three_lines_of_assertion_error_in_failing_step() {
        final AssertionError error = new AssertionError("Test assertion error");

        final String reportOutput = reportOutputWith(error);

        final int numOfElement = 3;
        final String stackTrace = ThrowableExtensions.convertToString(error, numOfElement);

        assertEquals(ansi().fg(YELLOW).a("Feature: Feature in error").reset().newline().
                fg(YELLOW).a("    Scenario: Scenario in error").reset().newline().
                fg(YELLOW).a("        Given an error step").reset().newline().
                a(stackTrace).toString(),
                reportOutput);
    }

    @Test
    public void report_should_print_out_the_link_to_the_registered_issue_when_error_occurs() {
        try(val ps = setSystemProperties("issueTrackerUrlPattern", "https://bitbucket.org/moresbycoffee/mbyhave8/issue/%s")) {

            val error = new AssertionError("Test assertion error");
            val reportOutput = reportOutputWith(error, asList("1"));
            val numOfElement = 3;
            val stackTrace = ThrowableExtensions.convertToString(error, numOfElement);

            assertEquals(ansi().fg(YELLOW).a("Feature: Feature in error").reset().newline().
                    fg(YELLOW).a("    Scenario: Scenario in error").reset().newline().
                    fg(YELLOW).a("        Given an error step").reset().newline().
                    a("Issue: https://bitbucket.org/moresbycoffee/mbyhave8/issue/1").newline().
                    a(stackTrace).toString(),
                    reportOutput);
        }
    }

    @Test
    public void report_should_print_out_all_the_links_to_issues_when_error_occurs() {
        try(val ps = setSystemProperties("issueTrackerUrlPattern", "https://bitbucket.org/moresbycoffee/mbyhave8/issue/%s")) {
            val error = new AssertionError("Test assertion error");
            val reportOutput = reportOutputWith(error, asList("1", "2"));
            val numOfElement = 3;
            //TODO convert back
            val stackTrace = ThrowableExtensions.convertToString(error, numOfElement);

            assertEquals(ansi().fg(YELLOW).a("Feature: Feature in error").reset().newline().
                    fg(YELLOW).a("    Scenario: Scenario in error").reset().newline().
                    fg(YELLOW).a("        Given an error step").reset().newline().
                    a("Issue: https://bitbucket.org/moresbycoffee/mbyhave8/issue/1").newline().
                    a("Issue: https://bitbucket.org/moresbycoffee/mbyhave8/issue/2").newline().
                    a(stackTrace).toString(),
                    reportOutput);
        }
    }

    @Test
    public void issues_reported_for_feature() {
        try(val ps = setSystemProperties("issueTrackerUrlPattern", "https://bitbucket.org/moresbycoffee/mbyhave8/issue/%s")) {
            val error = new AssertionError("Test assertion error");
            val reportOutput = failureReportOutputWith(error, asList("1"), asList("2"));
            val numOfElement = 3;
            //TODO convert back
            val stackTrace = ThrowableExtensions.convertToString(error, numOfElement);

            assertEquals(ansi().fg(YELLOW).a("Feature: Feature in error").reset().newline().
                    fg(YELLOW).a("    Scenario: Scenario in error").reset().newline().
                    fg(YELLOW).a("        Given an error step").reset().newline().
                    a("Issue: https://bitbucket.org/moresbycoffee/mbyhave8/issue/1").newline().
                    a("Issue: https://bitbucket.org/moresbycoffee/mbyhave8/issue/2").newline().
                    a(stackTrace).toString(),
                    reportOutput);
        }
    }

    private String reportOutputWith(final Throwable error) {
        return reportOutputWith(error, Collections.emptyList());
    }

    private String reportOutputWith(final Throwable error, final List<String> scenarioIssues) {
        if (error instanceof AssertionError) {
            return failureReportOutputWith((AssertionError) error, scenarioIssues);
        } else {
            return errorReportOutputWith(error, scenarioIssues);
        }
    }

    private String errorReportOutputWith(final Throwable error, final List<String> scenarioIssues) {
        return report(new SpecOutput(this.getClass().getName(), SpecResult.Error, asList(
                FeatureOutput.builder().
                        description("Feature: Feature in error").
                        result(FeatureResult.Error).
//                        issues(featureIssues).
                        scenarios(asList(
                                ScenarioOutput.builder().
                                description("Scenario: Scenario in error").
                                result(ScenarioResult.Error).
                                steps(asList(
                                        new StepOutput("Given an error step", StepResult.error(error))
                                )).
                                issues(scenarioIssues).build()
                        )).build()
                )
        ));
    }

    private String failureReportOutputWith(final AssertionError error, final List<String> scenarioIssues) {
        return failureReportOutputWith(error, Collections.emptyList(), scenarioIssues);
    }

    private String failureReportOutputWith(final AssertionError error, final List<String> featureIssues, final List<String> scenarioIssues) {
        return report(new SpecOutput(this.getClass().getName(), SpecResult.Failure, asList(
                FeatureOutput.builder().
                        description("Feature: Feature in error").
                        result(FeatureResult.Failure).
                        issues(featureIssues).
                        scenarios(asList(
                                ScenarioOutput.builder().
                                description("Scenario: Scenario in error").
                                result(ScenarioResult.Failure).
                                steps(asList(
                                        new StepOutput("Given an error step", StepResult.failure(error))
                                )).
                                issues(scenarioIssues).build()
                        )).build()
                )
        ));
    }

    private String report(final SpecOutput output) {
        final StringWriter outputWriter = new StringWriter();
        final Reporter reporter = new AnsiWriterReporter(outputWriter);
        reporter.report(output);
        return outputWriter.toString();
    }
}
