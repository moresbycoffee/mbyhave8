/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit;

import static org.junit.Assert.*;

import java.lang.reflect.Method;

import org.junit.Ignore;
import org.junit.Test;
import org.moresbycoffee.mbyhave8.annotations.AfterFeature;
import org.moresbycoffee.mbyhave8.annotations.AfterScenario;
import org.moresbycoffee.mbyhave8.annotations.AfterSpec;
import org.moresbycoffee.mbyhave8.annotations.AnnotationHookProcess;
import org.moresbycoffee.mbyhave8.annotations.BeforeFeature;
import org.moresbycoffee.mbyhave8.annotations.BeforeScenario;
import org.moresbycoffee.mbyhave8.annotations.BeforeSpec;
import org.moresbycoffee.mbyhave8.annotations.HookAnnotations;
import org.moresbycoffee.mbyhave8.result.FeatureResult;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;
import org.moresbycoffee.mbyhave8.structure.Specification;

public class AnnotationHookProcessorTest {

    public static class TestClass {

        @BeforeSpec
        public void beforeSpecificationMethod(final Specification spec) {
            /* No operation required */
        }

        @AfterSpec
        public void afterSpecificationMethod(final Specification spec, final SpecOutput output) {
            /* No operation required */
        }

        @BeforeFeature
        public void beforeFeatureMethod(final Feature feature) {
            /* No operation required */
        }

        @AfterFeature
        public void afterFeatureMethod(final Feature feature, final FeatureResult featureResult) {
            /* No operation required */
        }

        @BeforeScenario
        public void beforeScenarioMethod(final Scenario scenario) {
            /* No operation required */
        }

        @AfterScenario
        public void afterScenarioMethod(final Scenario scenario, final ScenarioResult scenarioResult) {
            /* No operation required */
        }

    }

    @Test
    public void should_add_before_specification_method_to_before_spec_hook() {
        final HookAnnotations annotations = AnnotationHookProcess.processClass(TestClass.class);
        final Method[] beforeSpecMethods = annotations.getBeforeSpecMethods();
        assertEquals("beforeSpecificationMethod", beforeSpecMethods[0].getName());
    }

    @Test
    public void should_add_after_specification_method_to_after_spec_hook() {
        final HookAnnotations annotations = AnnotationHookProcess.processClass(TestClass.class);
        final Method[] afterSpecMethods = annotations.getAfterSpecMethods();
        assertEquals("afterSpecificationMethod", afterSpecMethods[0].getName());
    }

    @Test
    public void should_add_before_feature_method_to_before_feature_hook() {
        final HookAnnotations annotations = AnnotationHookProcess.processClass(TestClass.class);
        final Method[] beforeFeatureMethods = annotations.getBeforeFeatureMethods();
        assertEquals("beforeFeatureMethod", beforeFeatureMethods[0].getName());
    }

    @Test
    public void should_add_after_feature_method_to_after_feature_hook() {
        final HookAnnotations annotations = AnnotationHookProcess.processClass(TestClass.class);
        final Method[] afterFeatureMethods = annotations.getAfterFeatureMethods();
        assertEquals("afterFeatureMethod", afterFeatureMethods[0].getName());
    }

    @Test
    public void should_add_before_scenario_method_to_before_scenario_hook() {
        final HookAnnotations annotations = AnnotationHookProcess.processClass(TestClass.class);
        final Method[] beforeScenarioMethods = annotations.getBeforeScenarioMethods();
        assertEquals("beforeScenarioMethod", beforeScenarioMethods[0].getName());
    }

    @Test
    public void should_add_after_scenario_method_to_after_scenario_hook() {
        final HookAnnotations annotations = AnnotationHookProcess.processClass(TestClass.class);
        final Method[] afterScenarioMethods = annotations.getAfterScenarioMethods();
        assertEquals("afterScenarioMethod", afterScenarioMethods[0].getName());
    }

    @Test @Ignore
    public void should_return_readable_error_if_the_parameters_are_not_valid() {
        fail();
    }


}
