/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit;

import static org.junit.Assert.*;

import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Test;
import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.result.StepOutput;
import org.moresbycoffee.mbyhave8.result.StepResult;
import org.moresbycoffee.mbyhave8.structure.Step;
import org.moresbycoffee.mbyhave8.unit.dummies.DummyStepHooks;

/**
 * Unit tests around And step declaration.
 */
public class And_step_Test {

    @Test
    public void and_step_execution_should_run_the_step_implementation() {
        final AtomicBoolean visited = new AtomicBoolean(false);

        final Step andStep = new MByHaveSpec() {
            Step innerStep = and("an And step", () -> { visited.set(true); return StepResult.Success; });
        }.innerStep;

        final StepOutput result = andStep.execute(DummyStepHooks.DUMMY);

        assertEquals(StepResult.Success, result.getResult());
        assertTrue("The and step execution should execute the step implementation and change the visited flag", visited.get());
    }

    @Test
    public void and_step_implemenentation_can_be_either_void_return_method() {
        final AtomicBoolean visited = new AtomicBoolean(false);

        final Step andStep = new MByHaveSpec() {
            Step innerStep = and("an And step", () -> visited.set(true));
        }.innerStep;

        final StepOutput result = andStep.execute(DummyStepHooks.DUMMY);

        assertEquals(StepResult.Success, result.getResult());
        assertTrue("The and step execution should execute the step implementation and change the visited flag", visited.get());
    }

    @Test
    public void implementationLess_and_step_should_return_pending_state() {
        final Step step = new MByHaveSpec() {
            Step innerStep = and("Implementationless Step");
        }.innerStep;

        final StepOutput stepOutput = step.execute(DummyStepHooks.DUMMY);

        assertEquals(StepResult.Pending, stepOutput.getResult());
    }

}
