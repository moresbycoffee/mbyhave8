/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit.hooks;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.hooks.MByHave8Hooks;
import org.moresbycoffee.mbyhave8.result.FeatureResult;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.result.StepResult;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;
import org.moresbycoffee.mbyhave8.structure.Specification;
import org.moresbycoffee.mbyhave8.structure.Step;

/**
 * Tests making sure {@link org.moresbycoffee.mbyhave8.hooks.MByHave8Hooks} called in time.
 */
public class CallbackInvocationTest {

    @Test
    public void spec_execution_should_invoke_callback_methods_in_order() {
        final MByHave8Hooks hooks = Mockito.mock(MByHave8Hooks.class);
        final InOrder inOrder = Mockito.inOrder(hooks);

        final MByHaveSpec spec = new MByHaveSpec() {{
            Feature("Test Feature1", Scenario("Scenario 1"));
        }};

        spec.registerHooks(hooks);

        spec.execute();

        inOrder.verify(hooks).startSpecification(Matchers.notNull(Specification.class));
        inOrder.verify(hooks).startFeature(Matchers.notNull(Feature.class));
        inOrder.verify(hooks).endFeature(Matchers.notNull(Feature.class), Matchers.eq(FeatureResult.Pending));
        inOrder.verify(hooks).endSpecification(Matchers.notNull(Specification.class), Matchers.notNull(SpecOutput.class));

    }

    @Test
    public void feature_start_callback_should_be_invoked_when_Feature_execution_starts() {
        final MByHave8Hooks hooks = Mockito.mock(MByHave8Hooks.class);
        final InOrder inOrder = Mockito.inOrder(hooks);

        final MByHaveSpec spec = new MByHaveSpec() {{
            Feature("Test Feature1", Scenario("Scenario 1"));
        }};

        spec.registerHooks(hooks);

        spec.execute();

        inOrder.verify(hooks).startFeature(Matchers.notNull(Feature.class));
        inOrder.verify(hooks).endFeature(Matchers.notNull(Feature.class), Matchers.eq(FeatureResult.Pending));

    }

    @Test
    public void feature_start_callback_should_not_be_invoked_when_Feature_is_empty() {
        final MByHave8Hooks hooks = Mockito.mock(MByHave8Hooks.class);

        final MByHaveSpec spec = new MByHaveSpec() {{
            Feature("Test Feature1");
        }};

        spec.registerHooks(hooks);

        spec.execute();

        verify(hooks, never()).startFeature(Matchers.notNull(Feature.class));
        verify(hooks, never()).endFeature(Matchers.notNull(Feature.class), Matchers.eq(FeatureResult.Pending));

    }

    @Test
    public void each_feature_start_callback_should_be_invoked_when_Feature_execution_starts() {
        final MByHave8Hooks hooks = Mockito.mock(MByHave8Hooks.class);
        final InOrder inOrder = Mockito.inOrder(hooks);

        final MByHaveSpec spec = new MByHaveSpec() {{
            Feature("Test Feature1", Scenario("Scenario 1"));
            Feature("Test Feature2", Scenario("Scenario 2"));
        }};

        spec.registerHooks(hooks);

        spec.execute();

        final ArgumentCaptor<Feature> featureCaptor = ArgumentCaptor.forClass(Feature.class);
        inOrder.verify(hooks).startFeature(featureCaptor.capture());
        inOrder.verify(hooks).endFeature(Matchers.eq(featureCaptor.getValue()), Matchers.eq(FeatureResult.Pending));
        inOrder.verify(hooks).startFeature(Matchers.notNull(Feature.class));
        inOrder.verify(hooks).endFeature(Matchers.notNull(Feature.class), Matchers.eq(FeatureResult.Pending));

    }

    @Test
    public void scenario_callbacks_should_be_invoked_when_scenarios_are_executed() {
        final MByHave8Hooks hooks = Mockito.mock(MByHave8Hooks.class);
        final InOrder inOrder = Mockito.inOrder(hooks);

        final MByHaveSpec spec = new MByHaveSpec() {{
            Feature("Test Feature1",
                    Scenario("Scenario1"),
                    Scenario("Scenario2"));
        }};

        spec.registerHooks(hooks);

        spec.execute();


        final ArgumentCaptor<Feature> featureCaptor = ArgumentCaptor.forClass(Feature.class);
        inOrder.verify(hooks).startFeature(featureCaptor.capture());
        assertNotNull(featureCaptor.getValue());
        inOrder.verify(hooks).startScenario(Matchers.eq(featureCaptor.getValue().getId()), Matchers.any(Scenario.class)); //TODO build matcher able to match description
        inOrder.verify(hooks).endScenario(Matchers.eq(featureCaptor.getValue().getId()), Matchers.any(Scenario.class), Matchers.eq(ScenarioResult.Pending));
        inOrder.verify(hooks).endFeature(Matchers.eq(featureCaptor.getValue()), Matchers.eq(FeatureResult.Pending));
    }

    @Test
    public void step_callbacks_should_be_invoked_when_steps_are_executed() {
        final MByHave8Hooks hooks = Mockito.mock(MByHave8Hooks.class);
        final InOrder inOrder = Mockito.inOrder(hooks);

        final MByHaveSpec spec = new MByHaveSpec() {{
            Feature("Test Feature1",
                    Scenario("Scenario1",
                        given("initial state", () -> {}),
                        when("something happens", () -> {}),
                        then("we get this state", () -> {})
                    )
            );
        }};

        spec.registerHooks(hooks);
        spec.execute();


        final ArgumentCaptor<Feature> featureCaptor = ArgumentCaptor.forClass(Feature.class);
        inOrder.verify(hooks).startFeature(featureCaptor.capture());
        assertNotNull(featureCaptor.getValue());

        final ArgumentCaptor<Scenario> scenarioCaptor = ArgumentCaptor.forClass(Scenario.class);
        inOrder.verify(hooks).startScenario(Matchers.eq(featureCaptor.getValue().getId()), scenarioCaptor.capture());
        assertNotNull(scenarioCaptor.getValue());

        final ArgumentCaptor<Step> stepCaptor = ArgumentCaptor.forClass(Step.class);
        inOrder.verify(hooks).startStep(Matchers.eq(featureCaptor.getValue().getId()), Matchers.eq(scenarioCaptor.getValue().getId()), stepCaptor.capture());
        inOrder.verify(hooks).endStep(Matchers.eq(featureCaptor.getValue().getId()), Matchers.eq(scenarioCaptor.getValue().getId()), Matchers.eq(stepCaptor.getValue()), Matchers.isA(StepResult.Success.class));

        inOrder.verify(hooks).startStep(Matchers.eq(featureCaptor.getValue().getId()), Matchers.eq(scenarioCaptor.getValue().getId()), stepCaptor.capture());
        inOrder.verify(hooks).endStep(Matchers.eq(featureCaptor.getValue().getId()), Matchers.eq(scenarioCaptor.getValue().getId()), Matchers.eq(stepCaptor.getValue()), Matchers.isA(StepResult.Success.class));


        inOrder.verify(hooks).endScenario(Matchers.eq(featureCaptor.getValue().getId()), Matchers.eq(scenarioCaptor.getValue()), Matchers.eq(ScenarioResult.Success));
        inOrder.verify(hooks).endFeature(Matchers.eq(featureCaptor.getValue()), Matchers.eq(FeatureResult.Success));
    }
}
