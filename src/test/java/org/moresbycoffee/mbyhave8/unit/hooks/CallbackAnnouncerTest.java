/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit.hooks;

import static org.mockito.Mockito.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.moresbycoffee.mbyhave8.hooks.CallbackAnnouncer;
import org.moresbycoffee.mbyhave8.hooks.MByHave8Hooks;
import org.moresbycoffee.mbyhave8.result.FeatureResult;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.result.StepResult;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;
import org.moresbycoffee.mbyhave8.structure.Specification;
import org.moresbycoffee.mbyhave8.structure.Step;

/**
 * Tests {@link CallbackAnnouncer}.
 */
@RunWith(MockitoJUnitRunner.class)
public class CallbackAnnouncerTest {

    @Mock private MByHave8Hooks hooks1;
    @Mock private MByHave8Hooks hooks2;

    private CallbackAnnouncer announcer;
    private InOrder inOrder;
    @Before
    public void setUp() {
        announcer = new CallbackAnnouncer();
        announcer.addListener(hooks1);
        announcer.addListener(hooks2);
        inOrder = inOrder(hooks1, hooks2);
    }

    private final Specification  specification  = mock(Specification.class);
    private final SpecOutput     specOutput     = mock(SpecOutput.class);
    private final Feature        feature        = mock(Feature.class);
    private final FeatureResult  featureResult  = FeatureResult.values()[new Random().nextInt(FeatureResult.values().length)];
    private final String         featureId      = "TEST FEAUTRE ID";
    private final Scenario       scenario       = mock(Scenario.class);
    private final ScenarioResult scenarioResult = ScenarioResult.values()[new Random().nextInt(ScenarioResult.values().length)];
    private final String         scenarioId     = "TEST SCENARIO ID";
    private final Step           step           = mock(Step.class);
    private final StepResult     stepResult     = mock(StepResult.class);

    @Test
    public void should_invoke_the_added_listeners_for_spec_events() {
        announcer.startSpecification(specification);
        verify(hooks1).startSpecification(specification);
        verify(hooks2).startSpecification(specification);
        announcer.endSpecification(specification, specOutput );
        verify(hooks1).endSpecification(specification, specOutput);
        verify(hooks2).endSpecification(specification, specOutput);
    }

    @Test
    public void should_invoke_the_added_listeners_for_feature_events() {
        announcer.startFeature(feature);
        verify(hooks1).startFeature(feature);
        verify(hooks2).startFeature(feature);
        announcer.endFeature(feature, featureResult);
        verify(hooks1).endFeature(feature, featureResult);
        verify(hooks2).endFeature(feature, featureResult);
    }

    @Test
    public void should_invoke_the_added_listeners_for_scenario_events() {
        announcer.startScenario(featureId, scenario);
        verify(hooks1).startScenario(featureId, scenario);
        verify(hooks2).startScenario(featureId, scenario);
        announcer.endScenario(featureId, scenario, scenarioResult);
        verify(hooks1).endScenario(featureId, scenario, scenarioResult);
        verify(hooks2).endScenario(featureId, scenario, scenarioResult);
    }

    @Test
    public void should_invoke_the_added_listeners_for_step_events() {
        announcer.startStep(featureId, scenarioId, step);
        verify(hooks1).startStep(featureId, scenarioId, step);
        verify(hooks2).startStep(featureId, scenarioId, step);
        announcer.endStep(featureId, scenarioId, step, stepResult);
        verify(hooks1).endStep(featureId, scenarioId, step, stepResult);
        verify(hooks2).endStep(featureId, scenarioId, step, stepResult);
    }

    @Test
    public void all_start_spec_hook_should_be_invoked_in_the_order_they_were_added() {
        announcer.startSpecification(specification);
        inOrder.verify(hooks1).startSpecification(specification);
        inOrder.verify(hooks2).startSpecification(specification);
    }

    @Test
    public void all_end_spec_hook_should_be_invoked_in_the_order_they_were_added() {
        announcer.endSpecification(specification, specOutput );
        inOrder.verify(hooks2).endSpecification(specification, specOutput);
        inOrder.verify(hooks1).endSpecification(specification, specOutput);
    }

    @Test
    public void all_start_feature_hook_should_be_invoked_in_the_order_they_were_added() {
        announcer.startFeature(feature);
        inOrder.verify(hooks1).startFeature(feature);
        inOrder.verify(hooks2).startFeature(feature);
    }

    @Test
    public void all_end_feature_end_hook_should_be_invoked_in_the_order_they_were_added() {
        announcer.endFeature(feature, featureResult);
        inOrder.verify(hooks2).endFeature(feature, featureResult);
        inOrder.verify(hooks1).endFeature(feature, featureResult);
    }

    @Test
    public void all_start_scenario_hook_should_be_invoked_in_the_order_they_were_added() {
        announcer.startScenario(featureId, scenario);
        inOrder.verify(hooks1).startScenario(featureId, scenario);
        inOrder.verify(hooks2).startScenario(featureId, scenario);
    }

    @Test
    public void all_end_scenario_hook_should_be_invoked_in_the_order_they_were_added() {
        announcer.endScenario(featureId, scenario, scenarioResult);
        inOrder.verify(hooks2).endScenario(featureId, scenario, scenarioResult);
        inOrder.verify(hooks1).endScenario(featureId, scenario, scenarioResult);
    }

    @Test
    public void all_start_step_hook_should_be_invoked_in_the_order_they_were_added() {
        announcer.startStep(featureId, scenarioId, step);
        inOrder.verify(hooks1).startStep(featureId, scenarioId, step);
        inOrder.verify(hooks2).startStep(featureId, scenarioId, step);
    }

    @Test
    public void all_end_step_hook_should_be_invoked_in_the_order_they_were_added() {
        announcer.endStep(featureId, scenarioId, step, stepResult);
        inOrder.verify(hooks2).endStep(featureId, scenarioId, step, stepResult);
        inOrder.verify(hooks1).endStep(featureId, scenarioId, step, stepResult);
    }



}
