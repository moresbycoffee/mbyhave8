/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.moresbycoffee.mbyhave8.unit.TestUtils.*;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Test;
import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.VoidStepImplementation;
import org.moresbycoffee.mbyhave8.result.ScenarioOutput;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.result.StepOutput;
import org.moresbycoffee.mbyhave8.result.StepResult;
import org.moresbycoffee.mbyhave8.structure.Scenario;
import org.moresbycoffee.mbyhave8.unit.dummies.DummyScenarioHooks;

/**
 * Created with IntelliJ IDEA.
 * User: envagyok
 * Date: 30/11/13
 * Time: 18:12
 * To change this template use File | Settings | File Templates.
 */
public class Scenario_execution_Test {

    @Test
    public void scenario_with_one_SUCCESS_step_should_be_SUCCESS() {
        assertEquals(ScenarioResult.Success, scenarioWith(successGivenStep()).execute(DummyScenarioHooks.DUMMY).getResult());
    }


    @Test
    public void scenario_output_should_carry_the_scenario_description() {
        assertEquals("Scenario: Scenario description", scenarioWith(successGivenStep()).execute(DummyScenarioHooks.DUMMY).getDescription());
    }


    @Test
    public void scenario_output_should_carry_the_step_outputs() {
        final Collection<StepOutput> steps = scenarioWith(successGivenStep()).execute(DummyScenarioHooks.DUMMY).getSteps();
        assertEquals(1, steps.size());
        assertEquals(StepResult.Success,      steps.iterator().next().getResult());
        assertEquals("Given Successful Given Step", steps.iterator().next().getDescription());
    }

    @Test
    public void scenario_with_one_PENDING_step_should_be_PENDING() {
        assertEquals(ScenarioResult.Pending, scenarioWith(pendingGivenStep()).execute(DummyScenarioHooks.DUMMY).getResult());
    }

    @Test
    public void scenario_with_one_FAILING_step_should_be_FAILING() {
        assertEquals(ScenarioResult.Failure, scenarioWith(failingGivenStep()).execute(DummyScenarioHooks.DUMMY).getResult());
    }

    @Test
    public void scenario_with_one_ERROR_step_should_be_ERROR() {
        assertEquals(ScenarioResult.Error, scenarioWith(errorGivenStep()).execute(DummyScenarioHooks.DUMMY).getResult());
    }

    @Test
    public void scenario_with_two_SUCCESS_step_should_be_SUCCESS() {
        assertEquals(ScenarioResult.Success, scenarioWith(successGivenStep(), successGivenStep()).execute(DummyScenarioHooks.DUMMY).getResult());
    }

    @Test
    public void scenario_with_a_SUCCESS_and_a_PENDING_step_should_be_pending() {
        assertEquals(ScenarioResult.Pending, scenarioWith(successGivenStep(), pendingGivenStep()).execute(DummyScenarioHooks.DUMMY).getResult());
    }

    @Test
    public void scenarion_without_any_steps_should_be_PENDING() {
        assertEquals(ScenarioResult.Pending, scenarioWith(/* no step */).execute(DummyScenarioHooks.DUMMY).getResult());
    }

//    @Test
//    public void notification_from_scenario_with_two_steps() {
//
//        RunNotifier notifier = mock(RunNotifier.class);
//        InOrder inOrder = inOrder(notifier);
//
//        final Scenario scenario = new MByHaveSpec() {
//            Scenario scenario = Scenario("scenario description",
//                    given("given step", () -> {}),
//                    then("then step", () -> {}));
//        }.scenario;
//
//        scenario.junitExecute(notifier, DummyScenarioHooks.DUMMY);
//
//        final Description scenarioDescription = Description.createSuiteDescription("Scenario: scenario description");
//
//        inOrder.verify(notifier).fireTestStarted(scenarioDescription);
//        inOrder.verify(notifier).fireTestStarted(argThat(descriptionStartingWith("Given given step")));
//        inOrder.verify(notifier).fireTestFinished(argThat(descriptionStartingWith("Given given step")));
//        inOrder.verify(notifier).fireTestStarted(argThat(descriptionStartingWith("Then then step")));
//        inOrder.verify(notifier).fireTestFinished(argThat(descriptionStartingWith("Then then step")));
//        inOrder.verify(notifier).fireTestFinished(scenarioDescription);
//
//
//
//    }

    @Test
    public void second_step_should_not_run_after_a_failing_step() {

        final AtomicBoolean isWhenVisited = new AtomicBoolean(false);
        final AtomicBoolean isThenVisited = new AtomicBoolean(false);

        final Scenario scenario = new MByHaveSpec() {
            Scenario scenario = Scenario("scenario description",
                    given("given step", (VoidStepImplementation) () -> { throw new AssertionError(); }),
                    when("when step", () -> { isWhenVisited.set(true); }),
                    then("then step", () -> { isThenVisited.set(true); }));
        }.scenario;

        final ScenarioOutput output = scenario.execute(DummyScenarioHooks.DUMMY);

        assertFalse("The when step should not run", isWhenVisited.get());
        assertFalse("The then step should not run", isThenVisited.get());

        assertEquals(ScenarioResult.Failure, output.getResult());
        assertThat("The Given step should fail", output.getSteps().get(0).getResult(), instanceOf(StepResult.Failure.class));
        assertThat("The When step should be skipped", output.getSteps().get(1).getResult(), instanceOf(StepResult.Skipped.class));
        assertThat("The Then step should be skipped", output.getSteps().get(1).getResult(), instanceOf(StepResult.Skipped.class));

    }

}
