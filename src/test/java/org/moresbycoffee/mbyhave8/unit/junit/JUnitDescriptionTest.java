/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit.junit;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.moresbycoffee.mbyhave8.unit.TestUtils.*;

import org.junit.Test;
import org.junit.runner.Description;
import org.moresbycoffee.mbyhave8.junit.JUnitDescription;
import org.moresbycoffee.mbyhave8.structure.Specification;
import org.moresbycoffee.mbyhave8.utils.ListExtensions;

import lombok.experimental.ExtensionMethod;

/**
 * TODO javadoc.
 */
@ExtensionMethod(ListExtensions.class)
public class JUnitDescriptionTest {

    @Test
    public void should_generate_a_maindescription() {


        final Specification specification = specificationWith(featureWith(scenarioWith(successGivenStep())));
        final Description rootDescription = JUnitDescription.parse(specification, this.getClass()).getRootDescription();

        assertThat(rootDescription, notNullValue());
        assertEquals(this.getClass().getName(), rootDescription.getClassName());
        final Description featureDescription = ListExtensions.head(rootDescription.getChildren());
        assertEquals("Feature: Feature description", featureDescription.getDisplayName());
        final Description scenarioDescription = ListExtensions.head(featureDescription.getChildren());
        assertEquals("Scenario: Scenario description", scenarioDescription.getDisplayName());
        //The end is the class of the test
        assertThat(ListExtensions.head(scenarioDescription.getChildren()).getDisplayName(), startsWith("Given Successful Given Step"));


    }
}
