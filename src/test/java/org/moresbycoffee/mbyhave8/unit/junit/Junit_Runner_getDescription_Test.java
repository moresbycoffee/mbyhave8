/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit.junit;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.moresbycoffee.mbyhave8.unit.TestUtils.*;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.Description;
import org.moresbycoffee.mbyhave8.junit.MByHave8Runner;

/**
 * TODO javadoc.
 */
public class Junit_Runner_getDescription_Test {

    @Test
    public void should_generate_description_for_single_feature_single_scenario_single_step() {

        final Description description = new MByHave8Runner(SimpleJunitCompatibleSpec.class).getDescription();

        assertEquals(SimpleJunitCompatibleSpec.class.getName(), description.getDisplayName());

        final ArrayList<Description> featureDescriptions = description.getChildren();
        assertThat(featureDescriptions.size(), equalTo(1));
        assertThat(featureDescriptions.get(0).getDisplayName(), is("Feature: Test feature"));

        final ArrayList<Description> scenarioDescriptions = featureDescriptions.get(0).getChildren();
        assertThat(scenarioDescriptions.size(), equalTo(1));
        assertThat(scenarioDescriptions.get(0).getDisplayName(), is("Scenario: Test scenario"));

        final ArrayList<Description> stepDescriptions = scenarioDescriptions.get(0).getChildren();
        assertThat(stepDescriptions.size(), equalTo(1));
        assertThat(stepDescriptions.get(0), descriptionStartingWith("Given Test given"));
    }

}
