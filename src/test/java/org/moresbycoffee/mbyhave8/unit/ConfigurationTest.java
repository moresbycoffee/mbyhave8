/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;
import org.moresbycoffee.mbyhave8.Configuration;
import org.moresbycoffee.mbyhave8.unit.utils.PropertySetter;

public class ConfigurationTest {

    @Test
    public void should_return_null_if_the_configuration_is_not_available() {
        assertNull(Configuration.getConfig("config.key"));
    }

    @Test
    public void should_return_the_configuration_from_System_properties() {
        try(PropertySetter ps = PropertySetter.setSystemProperties("config.key", "config.value")) {
            assertEquals("config.value", Configuration.getConfig("config.key"));
        }
    }

    @Test @Ignore
    public void should_return_the_configuration_from_config_file() {
        fail("I don't know how to implement this.");
    }

    @Test @Ignore
    public void system_properties_configuration_should_override_config_from_config_file() {
        fail("I don't know how to implement this.");
    }

}
