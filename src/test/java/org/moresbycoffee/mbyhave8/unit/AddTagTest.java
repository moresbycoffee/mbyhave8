/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.moresbycoffee.mbyhave8.unit.TestUtils.*;

import org.junit.Test;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;

/**
 * Tests if the tags are added to the scenario/feature.
 *
 * TODO these test should change as we move to immutable Feature/Scenario classes.
 */
@SuppressWarnings("all")
public class AddTagTest {

    @Test
    public void should_add_tag_to_scenario() {
        final Scenario scenario = successScenario();
        scenario.tag("TEST TAG1");
        assertThat(scenario.getTags(), contains("TEST TAG1"));
    }

    @Test
    public void should_add_all_tags_to_scenario() {
        final Scenario scenario = successScenario();
        scenario.tag("TEST TAG1").tag("TEST TAG2");
        assertThat(scenario.getTags(), contains("TEST TAG1", "TEST TAG2"));
    }

    @Test
    public void should_add_multiple_tags_to_scenario() {
        final Scenario scenario = successScenario();
        scenario.tag("TEST TAG1", "TEST TAG2");
        assertThat(scenario.getTags(), contains("TEST TAG1", "TEST TAG2"));
    }

    @Test
    public void should_trim_the_tags_on_scenarios() {
        final Scenario scenario = successScenario();
        scenario.tag("tag1   ", "   ", "   tag2", "tag3", null);
        assertThat(scenario.getTags(), contains("tag1", "tag2", "tag3"));
    }

    @Test
    public void should_should_not_do_anything_with_null_tag() {
        final Scenario scenario = successScenario();
        scenario.tag(null);
        assertTrue(scenario.getTags().isEmpty());
    }


    @Test
    public void should_add_issue_to_scenario() {
        final Scenario scenario = successScenario();
        scenario.issue("MBH-1");
        assertThat(scenario.getIssues(), contains("MBH-1"));
    }

    @Test
    public void should_add_multiple_issues_to_scenario() {
        final Scenario scenario = successScenario();
        scenario.issue("MBH-1", "MBH-2").issue("MBH-3");
        assertThat(scenario.getIssues(), contains("MBH-1", "MBH-2", "MBH-3"));
    }

    @Test
    public void should_handle_null_issue_on_scenario() {
        final Scenario scenario = successScenario();
        scenario.issue(null);
        assertTrue(scenario.getIssues().isEmpty());
    }

    @Test
    public void should_trim_the_issues_on_scenario() {
        final Scenario scenario = successScenario();
        scenario.issue("MBH-1   ", "   ", "   MBH-2", "MBH-3", null);
        assertThat(scenario.getIssues(), contains("MBH-1", "MBH-2", "MBH-3"));
    }

    @Test
    public void should_add_issues_to_tags_as_well_on_a_scenario() {
        final Scenario scenario = successScenario();
        scenario.tag("tag1").issue("MBH-1", "MBH-2");
        assertThat(scenario.getTags(), contains("tag1", "MBH-1", "MBH-2"));
    }

    @Test
    public void should_add_tag_to_feature() {
        final Feature feature = featureWith(successScenario());
        feature.tag("TEST TAG1");
        assertThat(feature.getTags(), contains("TEST TAG1"));
    }

    @Test
    public void should_add_all_tags_to_feature() {
        final Feature feature = featureWith(successScenario());
        feature.tag("TEST TAG1").tag("TEST TAG2");
        assertThat(feature.getTags(), contains("TEST TAG1", "TEST TAG2"));
    }

    @Test
    public void should_add_multiple_tags_to_feature() {
        final Feature feature = featureWith(successScenario());
        feature.tag("TEST TAG1", "TEST TAG2");
        assertThat(feature.getTags(), contains("TEST TAG1", "TEST TAG2"));
    }

    @Test
    public void should_trim_the_tags_on_feature() {
        final Feature feature = featureWith(successScenario());
        feature.tag("tag1   ", "   ", "   tag2", "tag3", null);
        assertThat(feature.getTags(), contains("tag1", "tag2", "tag3"));
    }

    @Test
    public void should_should_not_do_anything_with_null_tag_to_feature() {
        final Feature feature = featureWith(successScenario());
        feature.tag(null);
        assertTrue(feature.getTags().isEmpty());
    }

    @Test
    public void should_add_issue() {
        final Feature feature = featureWith(successScenario());
        feature.issue("MBH-1");
        assertThat(feature.getIssues(), contains("MBH-1"));
    }

    @Test
    public void should_add_multiple_issues() {
        final Feature feature = featureWith(successScenario());
        feature.issue("MBH-1", "MBH-2").issue("MBH-3");
        assertThat(feature.getIssues(), contains("MBH-1", "MBH-2", "MBH-3"));
    }

    @Test
    public void should_handle_null_issue() {
        final Feature feature = featureWith(successScenario());
        feature.issue(null);
        assertTrue(feature.getIssues().isEmpty());
    }

    @Test
    public void should_trim_the_issues() {
        final Feature feature = featureWith(successScenario());
        feature.issue("MBH-1   ", "   ", "   MBH-2", "MBH-3", null);
        assertThat(feature.getIssues(), contains("MBH-1", "MBH-2", "MBH-3"));
    }

    @Test
    public void should_add_issues_to_tags_as_well_on_a_feature() {
        final Feature feature = featureWith(successScenario());
        feature.tag("tag1").issue("MBH-1", "MBH-2");
        assertThat(feature.getTags(), contains("tag1", "MBH-1", "MBH-2"));
    }

}
