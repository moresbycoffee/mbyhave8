/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit;


import org.junit.Test;
import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.result.StepResult;
import org.moresbycoffee.mbyhave8.result.StepOutput;
import org.moresbycoffee.mbyhave8.structure.Step;
import org.moresbycoffee.mbyhave8.unit.dummies.DummyStepHooks;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.moresbycoffee.mbyhave8.unit.TestUtils.*;

/**
 * Created with IntelliJ IDEA.
 * User: envagyok
 * Date: 30/11/13
 * Time: 17:36
 * To change this template use File | Settings | File Templates.
 */
public class Then_step_Test {

    @Test
    public void then_step_implementation_without_return_value_should_AssertException_as_failure() {
        StepOutput stepOutput = failingThenStep().execute(DummyStepHooks.DUMMY);
        assertThat(stepOutput.getResult(), instanceOf(StepResult.Failure.class));
    }

    @Test
    public void then_step_implementation_without_return_value_should_PendingException_as_pending() {
        StepOutput stepOutput = pendingThenStep().execute(DummyStepHooks.DUMMY);
        assertEquals(StepResult.Pending, stepOutput.getResult());
    }

    @Test
    public void then_step_implementation_without_return_value_should_any_other_Exception_as_error() {
        StepOutput stepOutput = errorThenStep().execute(DummyStepHooks.DUMMY);
        assertThat(stepOutput.getResult(), instanceOf(StepResult.Error.class));
    }

    @Test
    public void implementationLess_then_step_should_return_pending_state() {
        final Step step = new MByHaveSpec() {
            Step innerStep = then("Implementationless Step");
        }.innerStep;

        final StepOutput stepOutput = step.execute(DummyStepHooks.DUMMY);

        assertEquals(StepResult.Pending, stepOutput.getResult());
    }

}
