/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.filter.Filter;
import org.moresbycoffee.mbyhave8.hooks.CallbackAnnouncer;
import org.moresbycoffee.mbyhave8.report.NullReporter;
import org.moresbycoffee.mbyhave8.result.FeatureResult;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.result.SpecResult;

/**
 * Created by envagyok on 14/03/14.
 */
public class Spec_execution_Test {

    @Test
    public void should_return_SUCCESS_when_the_only_features_result_is_succes() {
        final MByHaveSpec spec = new MByHaveSpec() {{
            Feature("this is a feature without runner",
                    Scenario("this is a scenario",
                            given("something", () -> {})));
        }};

        assertEquals(SpecResult.Success, spec.execute().getResult());
    }

    @Test
    public void should_return_the_name_of_the_class_in_the_spec_output() {
        final MByHaveSpec spec = new MByHaveSpec() {{
            Feature("this is a feature without runner",
                    Scenario("this is a scenario",
                            given("something", () -> {
                            })));
        }};

        assertThat(spec.execute().getTestClassName(), startsWith(this.getClass().getName()));
    }

    @Test
    public void should_return_the_outputs_of_the_underlying_features() {
        final MByHaveSpec spec = new MByHaveSpec() {{
            Feature("this is a feature without runner",
                    Scenario("this is a scenario",
                            given("something", () -> {
                            })));
        }};

        final SpecOutput output = spec.execute();

        assertEquals(1, output.getFeatures().size());
        assertEquals(FeatureResult.Success, output.getFeatures().iterator().next().getResult());

    }

    @Test
    public void should_not_execute_not_executable_features() {
        final MByHaveSpec spec = new MByHaveSpec() {{
            Feature("this is a feature without runner",
                Scenario("this is a scenario",
                    given("something", () -> {})
                )
            ).tag("Broken");
        }};

        final SpecOutput output = spec.getSpecification().execute(
                spec.getClass(), new NullReporter(), new CallbackAnnouncer(),
                new Filter("~Broken"));

        assertEquals(0, output.getFeatures().size());
    }


}
