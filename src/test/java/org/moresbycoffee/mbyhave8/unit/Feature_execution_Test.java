/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.unit;

import static org.junit.Assert.*;
import static org.moresbycoffee.mbyhave8.unit.TestUtils.*;

import org.junit.Test;
import org.moresbycoffee.mbyhave8.filter.Filter;
import org.moresbycoffee.mbyhave8.result.FeatureOutput;
import org.moresbycoffee.mbyhave8.result.FeatureResult;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.unit.dummies.DummyHooks;

/**
 * Created with IntelliJ IDEA.
 * User: envagyok
 * Date: 30/11/13
 * Time: 18:42
 * To change this template use File | Settings | File Templates.
 */
public class Feature_execution_Test {

    @Test
    public void feature_with_one_SUCCESS_scenario_should_be_SUCCESS() {
        assertEquals(FeatureResult.Success, featureWith(successScenario()).execute(DummyHooks.DUMMY, Filter.EMPTY_FILTER).getResult());
    }

    @Test
    public void feature_output_should_hold_the_feature_description() {
        assertEquals("Feature: Feature description", featureWith(successScenario()).execute(DummyHooks.DUMMY, Filter.EMPTY_FILTER).getDescription());
    }

    @Test
    public void feature_output_should_carry_the_underlying_scenario_outputs() {
        final FeatureOutput featureOutput = featureWith(successScenario()).execute(DummyHooks.DUMMY, Filter.EMPTY_FILTER);
        assertEquals(1, featureOutput.getScenarios().size());
        assertEquals(ScenarioResult.Success, featureOutput.getScenarios().iterator().next().getResult());
    }


    @Test
    public void feature_with_one_SUCCESS_and_one_PENDING_scenario_should_be_PENDING() {
        assertEquals(FeatureResult.Pending, featureWith(successScenario(), pendingScenario()).execute(DummyHooks.DUMMY, Filter.EMPTY_FILTER).getResult());
    }

    @Test
    public void feature_with_one_SUCCESS_an_ERROR_and_one_PENDING_scenario_should_be_ERROR() {
        assertEquals(FeatureResult.Error, featureWith(successScenario(), errorScenario(), pendingScenario()).execute(DummyHooks.DUMMY, Filter.EMPTY_FILTER).getResult());
    }

    @Test
    public void feature_without_any_scenario_should_be_PENDING() {
        assertEquals(FeatureResult.Pending, featureWith(/* no scenario */).execute(DummyHooks.DUMMY, Filter.EMPTY_FILTER).getResult());
    }

    @Test
    public void a_feature_with_a_pending_and_an_failing_scenario_should_be_in_ERROR_state() {
        assertEquals(FeatureResult.Error, featureWith(failingScenario(), errorScenario()).execute(DummyHooks.DUMMY, Filter.EMPTY_FILTER).getResult());
        assertEquals(FeatureResult.Error, featureWith(errorScenario(), failingScenario()).execute(DummyHooks.DUMMY, Filter.EMPTY_FILTER).getResult());
    }

    @Test
    public void a_feature_with_a_failing_and_an_error_scenario_should_be_in_FAILURE_state() {
        assertEquals(FeatureResult.Failure, featureWith(pendingScenario(), failingScenario()).execute(DummyHooks.DUMMY, Filter.EMPTY_FILTER).getResult());
        assertEquals(FeatureResult.Failure, featureWith(failingScenario(), pendingScenario()).execute(DummyHooks.DUMMY, Filter.EMPTY_FILTER).getResult());
    }
}
