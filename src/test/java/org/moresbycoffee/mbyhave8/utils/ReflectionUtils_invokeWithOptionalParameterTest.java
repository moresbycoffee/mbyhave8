/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.utils;

import static org.junit.Assert.*;
import static org.moresbycoffee.mbyhave8.utils.ReflectionUtils.*;

import org.junit.Before;
import org.junit.Test;

import lombok.Getter;

public class ReflectionUtils_invokeWithOptionalParameterTest {

    private TestClass testObject;
    private Class<TestClass> testClass;

    @Getter
    public static class TestClass {

        private boolean noParameterMethod = false;
        private boolean singleParameterMethod = false;


        public void noParameterMethod() {
            noParameterMethod = true;
        }

        public String noParameterMethodWithReturnValue() {
            return "noParameterMethodWithReturnValue";
        }

        public void singleParameterMethod(final String input) {
            singleParameterMethod = true;
        }

        public String sameTypeParametersMethod(final String input1, final String input2) {
            return input1.concat(input2);
        }
    }

    @Before
    public void setUp() {
        testObject = new TestClass();
        testClass = TestClass.class;
    }

    @Test
    public void should_invoke_no_parameter_method_if_there_are_no_parameters_provided() throws Exception {
        assertNull(invokeWithOptionalParameter(testClass.getMethod("noParameterMethod"), testObject));
        assertTrue(testObject.isNoParameterMethod());
    }

    @Test
    public void should_invoke_no_parameter_method_if_there_are_no_parameters_provided_and_return_value() throws Exception {
        final Object result = invokeWithOptionalParameter(testClass.getMethod("noParameterMethodWithReturnValue"), testObject);
        assertEquals("noParameterMethodWithReturnValue", result);
    }

    @Test
    public void should_invoke_no_parameter_method_even_if_there_are_parameters_provided() throws Exception {
        final Object[] parameters = new Object[] { "String param", Integer.valueOf(143) };
        assertNull(invokeWithOptionalParameter(testClass.getMethod("noParameterMethod"), testObject, parameters));
        assertTrue(testObject.isNoParameterMethod());
    }

    @Test
    public void should_invoke_singe_parameter_method() throws Exception {
        final Object[] parameters = new Object[] { "String parameter" };
        assertNull(invokeWithOptionalParameter(testClass.getMethod("singleParameterMethod", String.class), testObject, parameters));
        assertTrue(testObject.isSingleParameterMethod());
    }

    @Test
    public void should_invoke_single_parameter_method_with_more_than_one_parameter() throws Exception {
        final Object[] parameters = new Object[] { "String parameter", Integer.valueOf(3434) };
        assertNull(invokeWithOptionalParameter(testClass.getMethod("singleParameterMethod", String.class), testObject, parameters));
        assertTrue(testObject.isSingleParameterMethod());
    }

    @Test
    public void should_invoke_single_parameter_method_with_more_than_one_parameter2() throws Exception {
        final Object[] parameters = new Object[] { Integer.valueOf(3434), "String parameter" };
        assertNull(invokeWithOptionalParameter(testClass.getMethod("singleParameterMethod", String.class), testObject, parameters));
        assertTrue(testObject.isSingleParameterMethod());
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_if_there_is_no_matching_parameter() throws Exception {
        final Object[] parameters = new Object[] { Integer.valueOf(3434), Double.valueOf(34214.342d) };
        invokeWithOptionalParameter(testClass.getMethod("singleParameterMethod", String.class), testObject, parameters);
    }
}
