/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.acceptance;

import static org.junit.Assert.*;

import java.util.concurrent.atomic.AtomicBoolean;

import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.annotations.AfterFeature;
import org.moresbycoffee.mbyhave8.annotations.AfterScenario;
import org.moresbycoffee.mbyhave8.annotations.AfterSpec;
import org.moresbycoffee.mbyhave8.annotations.BeforeFeature;
import org.moresbycoffee.mbyhave8.annotations.BeforeScenario;
import org.moresbycoffee.mbyhave8.annotations.BeforeSpec;
import org.moresbycoffee.mbyhave8.result.FeatureResult;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;
import org.moresbycoffee.mbyhave8.structure.Specification;

public class HookTestSpec extends MByHaveSpec {

    public static final ThreadLocal<AtomicBoolean> flag           = new ThreadLocal<AtomicBoolean>();
    public static final ThreadLocal<AtomicBoolean> beforeSpec     = new ThreadLocal<AtomicBoolean>();
    public static final ThreadLocal<AtomicBoolean> afterSpec      = new ThreadLocal<AtomicBoolean>();
    public static final ThreadLocal<AtomicBoolean> beforeFeature  = new ThreadLocal<AtomicBoolean>();
    public static final ThreadLocal<AtomicBoolean> afterFeature   = new ThreadLocal<AtomicBoolean>();
    public static final ThreadLocal<AtomicBoolean> beforeScenario = new ThreadLocal<AtomicBoolean>();
    public static final ThreadLocal<AtomicBoolean> afterScenario  = new ThreadLocal<AtomicBoolean>();

    public static final ThreadLocal<AtomicBoolean> noParamBeforeSpec     = new ThreadLocal<AtomicBoolean>();
    public static final ThreadLocal<AtomicBoolean> noParamAfterSpec      = new ThreadLocal<AtomicBoolean>();
    public static final ThreadLocal<AtomicBoolean> noParamBeforeFeature  = new ThreadLocal<AtomicBoolean>();
    public static final ThreadLocal<AtomicBoolean> noParamAfterFeature   = new ThreadLocal<AtomicBoolean>();
    public static final ThreadLocal<AtomicBoolean> noParamBeforeScenario = new ThreadLocal<AtomicBoolean>();
    public static final ThreadLocal<AtomicBoolean> noParamAfterScenario  = new ThreadLocal<AtomicBoolean>();


    {
        Feature("this is a feature in the runner",
            Scenario("this is a scenario",
                given("something", this::givenStepImplementation)));
    }


    void givenStepImplementation() {
        flag.get().set(true);
    }

    @BeforeSpec
    public void beforeSpecification(final Specification spec) throws Exception {
        assertNotNull(spec);
        beforeSpec.get().set(true);
    }

    @AfterSpec
    public void afterSpecification(final Specification specification, final SpecOutput output) {
        assertNotNull(specification);
        assertNotNull(output);
        afterSpec.get().set(true);
    }

    @BeforeFeature
    public void beforeFeature(final Feature feature) {
        assertNotNull(feature);
        beforeFeature.get().set(true);
    }

    @AfterFeature
    public void afterFeature(final Feature feature, final FeatureResult result) {
        assertNotNull(feature);
        assertNotNull(result);
        afterFeature.get().set(true);
    }

    @BeforeScenario
    public void beforeScenario(final Scenario scenario) {
        assertNotNull(scenario);
        beforeScenario.get().set(true);
    }

    @AfterScenario
    public void afterScenario(final Scenario scenario, final ScenarioResult result) {
        assertNotNull(scenario);
        assertNotNull(result);
        afterScenario.get().set(true);
    }

    @BeforeSpec
    public void noParamBeforeSpecification() {
        noParamBeforeSpec.get().set(true);
    }

    @AfterSpec
    public void noParamAfterSpecification() {
        noParamAfterSpec.get().set(true);
    }

    @BeforeFeature
    public void noParamBeforeFeature() {
        noParamBeforeFeature.get().set(true);
    }

    @AfterFeature
    public void noParamAfterFeature() {
        noParamAfterFeature.get().set(true);
    }

    @BeforeScenario
    public void noParamBeforeScenario() {
        noParamBeforeScenario.get().set(true);
    }

    @AfterScenario
    public void noParamAfterScenario() {
        noParamAfterScenario.get().set(true);
    }
}