/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.acceptance;

import static org.junit.Assert.*;

import java.util.concurrent.atomic.AtomicBoolean;

import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.guice.annotations.GuiceInjection;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provider;

import lombok.Data;

/**
 * Created by envagyok on 29/03/14.
 */
@GuiceInjection(GuiceCustomModuleTest.GuiceCustomModule.class)
public class GuiceCustomModuleTest extends MByHaveSpec {
    {
        Feature("GuiceInjection annotation to be able to define custom module",
                Scenario("Simple scenario with a simple bean provider",
                    when("myBean is used", this::assertMyBeanIsInjected),
                    then("the provider should be in use", this::assertProviderHasBeenUsed)
        ));

    }

    @Inject MyBean myBean;

    void assertMyBeanIsInjected() {
        assertNotNull(myBean);
        assertEquals("This is a testString", myBean.getTestString());
    }

    void assertProviderHasBeenUsed() {
        assertTrue(providerInUse.get());
        //reset
        providerInUse.set(false);
    }

    private static final AtomicBoolean providerInUse = new AtomicBoolean(false);

    public static final Provider<MyBean> myBeanProvider = new Provider<MyBean>() {
        @Override
        public MyBean get() {
            providerInUse.set(true);
            return new MyBean();
        }
    };

    @Data
    public static class MyBean {
        public final String testString = "This is a testString";
    }

    public static class GuiceCustomModule extends AbstractModule {
        @Override
        protected void configure() {
            bind(MyBean.class).toProvider(myBeanProvider);
        }
    }


}
