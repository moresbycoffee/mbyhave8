/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.acceptance;

import static org.junit.Assert.*;

import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.moresbycoffee.mbyhave8.MByHaveSpec;

/**
 * Acceptance test around tags and filters.
 */
public class TagSupportTest {

    public static class SimpleTagSupport extends MByHaveSpec{

        static final ThreadLocal<AtomicBoolean> scenario1 = new ThreadLocal<>();
        static final ThreadLocal<AtomicBoolean> scenario2 = new ThreadLocal<>();
        static final ThreadLocal<AtomicBoolean> scenario3 = new ThreadLocal<>();

        {
            Feature("Tag support feature",
                Scenario("the first case",
                    given("step", () -> scenario1.get().set(true))
                ).tag("tag1"),
                Scenario("the second case",
                    given("step", () -> scenario2.get().set(true))
                ).tag("tag2")
            );
            Feature("Second feature",
                Scenario("The third case",
                    given("step", () -> scenario3.get().set(true))
                ).tag("tag3")
            ).tag("featureTag");
        }

        public static void reset() {
            scenario1.set(new AtomicBoolean(false));
            scenario2.set(new AtomicBoolean(false));
            scenario3.set(new AtomicBoolean(false));
        }

    }

    public String mbyhaveTagsProp;

    @Before
    public void reset() {
        mbyhaveTagsProp = System.getProperty("mbyhave.tags");
        SimpleTagSupport.reset();
    }
    @After
    public void resetSystemProps() {
        System.setProperty("mbyhave.tags", mbyhaveTagsProp == null ? "" : mbyhaveTagsProp);
    }

    @Test
    public void tag_addition() {
        final Result result = new JUnitCore().run(new Class[] {SimpleTagSupport.class});
        assertTrue(SimpleTagSupport.scenario1.get().get());
        assertTrue(SimpleTagSupport.scenario2.get().get());
        assertTrue(result.wasSuccessful());
    }

    @Test
    public void should_run_only_scenario1_because_of_the_filter() {
        System.setProperty("mbyhave.tags", "tag1");
        final Result result = new JUnitCore().run(new Class[] {SimpleTagSupport.class});
        assertTrue(SimpleTagSupport.scenario1.get().get());
        assertFalse(
                "The second scenario shouldn't be executed because the tag is not selected",
                SimpleTagSupport.scenario2.get().get());
        assertTrue(result.wasSuccessful());
    }

    @Test
    public void should_not_run_scenario1_because_of_the_filter() {
        System.setProperty("mbyhave.tags", "~tag1");
        final Result result = new JUnitCore().run(new Class[] {SimpleTagSupport.class});
        assertFalse(
                "The first scenario shouldn't be executed because of the exclusion filter.",
                SimpleTagSupport.scenario1.get().get());
        assertTrue(SimpleTagSupport.scenario2.get().get());
        assertTrue(result.wasSuccessful());
    }

    @Test
    public void should_not_run_feature1_because_of_the_filter() {
        System.setProperty("mbyhave.tags", "featureTag");
        final Result result = new JUnitCore().run(new Class[] {SimpleTagSupport.class});
        assertFalse(
                "The first feature shouldn't be executed because of the exclusion filter.",
                SimpleTagSupport.scenario1.get().get());
        assertFalse(SimpleTagSupport.scenario2.get().get());
        assertTrue(SimpleTagSupport.scenario3.get().get());
        assertTrue(result.wasSuccessful());
    }



    @Test
    public void should_run_feature1_because_of_the_filter() {
        System.setProperty("mbyhave.tags", "~featureTag");
        final Result result = new JUnitCore().run(new Class[] {SimpleTagSupport.class});
        assertTrue(
                "The first feature should be executed because of the exclusion filter.",
                SimpleTagSupport.scenario1.get().get());
        assertTrue(SimpleTagSupport.scenario2.get().get());
        assertFalse(SimpleTagSupport.scenario3.get().get());
        assertTrue(result.wasSuccessful());
    }

    @Test
    public void should_not_run_and_report_any_test_if_all_scenario_filtered_out() {
        System.setProperty("mbyhave.tags", "~featureTag, ~tag1, ~tag2");
        final Result result = new JUnitCore().run(new Class[] {SimpleTagSupport.class});
        assertEquals(0, result.getRunCount());

    }


}
