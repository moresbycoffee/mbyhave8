/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.acceptance;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.moresbycoffee.mbyhave8.unit.utils.PropertySetter.*;

import java.io.StringWriter;

import org.junit.Test;
import org.moresbycoffee.mbyhave8.MByHaveSpec;

import lombok.val;

/**
 * Created by envagyok on 13/03/14.
 */
public class OutputWithoutRunnerTest {

    @Test
    public void a_successful_feature_result_should_be_printed_out_in_green() {

        val testOutput = new StringWriter();
        val spec = new MByHaveSpec() {
            {
                Feature("this is a feature without runner",
                    Scenario("this is a scenario",
                        given("something", this::givenStepImplementation)));
            }

            void givenStepImplementation() {
                //Doing nothing
            }
        };

        spec.execute(testOutput);

        assertEquals(ansi().fg(GREEN).a("Feature: this is a feature without runner").reset().newline().
                            fg(GREEN).a("    Scenario: this is a scenario").reset().newline().
                            fg(GREEN).a("        Given something").reset().newline().toString(),
                     testOutput.toString());
    }

    @Test
    public void should_report_issue_if_the_test_fails() {

        try(val ps = setSystemProperties("issueTrackerUrlPattern", "https://bitbucket.org/moresbycoffee/mbyhave8/issue/%s")) {

            val testOutput = new StringWriter();
            val spec = new MByHaveSpec() {{

                Feature("this is a feature without runner",
                    Scenario("this is a scenario",
                        given("something", () -> fail())
                    )
                ).issue("7");

            }};

            spec.execute(testOutput);

            assertThat(testOutput.toString(),
                       startsWith(ansi().fg(YELLOW).a("Feature: this is a feature without runner").reset().newline().
                                         fg(YELLOW).a("    Scenario: this is a scenario").reset().newline().
                                         fg(YELLOW).a("        Given something").reset().newline().
                                         a("Issue: https://bitbucket.org/moresbycoffee/mbyhave8/issue/7").newline().
                                         a("java.lang.AssertionError").newline().
                                         toString()
                                  )
                       );
        }
    }
}
