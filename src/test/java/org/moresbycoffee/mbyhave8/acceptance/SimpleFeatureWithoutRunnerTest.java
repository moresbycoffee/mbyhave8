/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.acceptance;

import static org.junit.Assert.*;

import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Test;
import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.PendingException;
import org.moresbycoffee.mbyhave8.VoidStepImplementation;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.result.SpecResult;

import lombok.val;

/**
 * TODO doc.
 */
public class SimpleFeatureWithoutRunnerTest {

    @Test
    public void feature_with_one_scenario_and_one_SUCCESS_step_should_be_SUCCESS_format1() {
        val flag = new AtomicBoolean(false);
        val spec = new MByHaveSpec() {
            {
                Feature("this is a feature in the runner",
                    Scenario("this is a scenario",
                        given("something", this::givenStepImplementation)));
            }

            void givenStepImplementation() {
                flag.set(true);
            }
        };

        assertFalse(flag.get());
        val output = spec.execute();
        assertTrue(flag.get());

        assertEquals(SpecResult.Success, output.getResult());
    }

    @Test
    public void feature_with_one_scenario_and_one_SUCCESS_step_should_be_SUCCESS_format2() {
        val hasGivenBeenVisited = new AtomicBoolean(false);
        val spec = new MByHaveSpec() {{
            Feature("this is a feature in the runner",
                Scenario("this is a scenario",
                    given("something", () -> { hasGivenBeenVisited.set(true); })));
        }};

        assertFalse(hasGivenBeenVisited.get());
        val output = spec.execute();
        assertTrue(hasGivenBeenVisited.get());
        assertEquals(SpecResult.Success, output.getResult());
    }


    @Test
    public void feature_with_one_scenario_and_one_PENDING_step_should_be_PENDING() {
        final MByHaveSpec spec = new MByHaveSpec() {{
            Feature("this is a pending feature",
                Scenario("this is a scenario",
                    given("something", (VoidStepImplementation) () -> { throw new PendingException(); })));
        }};

        final SpecOutput output = spec.execute();

        assertEquals(SpecResult.Pending, output.getResult());
    }



    @Test
    public void spec_with_one_SUCCESS_and_one_PENDING_feature_should_be_PENDING() {
        final MByHaveSpec spec = new MByHaveSpec() {{

            Feature("this is a success feature",
                    Scenario("this is a scenario",
                            given("something", () -> { })));


            Feature("this is a pending feature",
                    Scenario("this is a scenario",
                            given("something", (VoidStepImplementation) () -> { throw new PendingException(); })));
        }};

        final SpecOutput output = spec.execute();

        assertEquals(SpecResult.Pending, output.getResult());
    }

    @Test
    public void feature_with_one_FULL_SUCCESS_scenario() {
        final AtomicBoolean givenStepVisitedFlag = new AtomicBoolean(false);
        final AtomicBoolean whenStepVisitedFlag = new AtomicBoolean(false);
        final AtomicBoolean thenStepVisitedFlag = new AtomicBoolean(false);

        final MByHaveSpec spec = new MByHaveSpec() {{

                Feature("this is a feature in the runner",
                    Scenario("this is a scenario",
                        given("something",                         this::givenStepImplementation),
                        when ("something happens",                 this::whenStepImplementation),
                        then ("something should be in some state", this::thenStepImplementation)
                    ));
            }

            void givenStepImplementation() { givenStepVisitedFlag.set(true); }
            void whenStepImplementation()  { whenStepVisitedFlag.set(true); }
            void thenStepImplementation()  { thenStepVisitedFlag.set(true); }
        };

        assertFalse(givenStepVisitedFlag.get() || whenStepVisitedFlag.get() || thenStepVisitedFlag.get());
        final SpecOutput output = spec.execute();
        assertTrue(givenStepVisitedFlag.get() && whenStepVisitedFlag.get() && thenStepVisitedFlag.get());

        assertEquals(SpecResult.Success, output.getResult());
    }

}
