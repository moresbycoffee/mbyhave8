/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.acceptance;

import lombok.Data;
import lombok.Getter;
import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.guice.annotations.FeatureScoped;
import org.moresbycoffee.mbyhave8.guice.annotations.ScenarioScoped;
import org.moresbycoffee.mbyhave8.guice.annotations.StepScoped;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;

/**
 * Tests around {@link org.moresbycoffee.mbyhave8.guice.annotations.StepScoped} bean injection.
 */
public class GuiceStepScopeTest extends MByHaveSpec {
    {
        Feature("first feature",
                Scenario("first scenarion",
                        given("a step increasing the feature, scenario and step count", this::count),
                        then("everything should be increased apart from the step count", () -> this.assertCounters(1, 1, 1, 0))),
                Scenario("second scenarion what should reset the scenario count",
                        given("a step increasing the counters", this::count),
                        then("counter and feature should be increased but scenario should be only 1", () -> this.assertCounters(2, 2, 1, 0)),
                        then("an other step increasing the counters", this::count),
                        then("the step should still stay 0", () -> this.assertCounters(3, 3, 2, 0))
        ));
        Feature("second feature what will reset the feature count",
                Scenario("first scenario in the second feature",
                        given("an other step increasing the counters", this::count),
                        then("the apart from the counter everything should show after reset state", () -> this.assertCounters(4, 1, 1, 0))
        ));
    }

    private int counter = 0;

    @Inject private FeatureBean featureBean;
    @Inject private ScenarioBean scenarioBean;
    @Inject private StepBean stepBean;

    @FeatureScoped public static class FeatureBean {
        @Getter private int number = 0;
        public void inc() { number += 1; }
    }

    @Data @StepScoped public static class StepBean {
        @Getter private int number = 0;
        public void inc() { number += 1; }
    }

    @Data @ScenarioScoped public static class ScenarioBean {
        @Getter private int number = 0;
        public void inc() { number += 1; }
    }


    void count() {
        counter++;
        featureBean.inc();
        scenarioBean.inc();
        stepBean.inc();
    }


    void assertCounters(int counterCount, int featureCount, int scenarioCount, int stepCount) {
        assertEquals(stepCount, stepBean.getNumber());
        assertEquals(scenarioCount, scenarioBean.getNumber());
        assertEquals(featureCount, featureBean.getNumber());
        assertEquals(counterCount, counter);
    }
}
