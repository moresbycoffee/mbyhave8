/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.acceptance;

import static org.junit.Assert.*;

import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

/**
 * Tests the hooks defined by annotations.
 */
public class AnnotationHooksTest {

    @Before
    public void setUp() {
        HookTestSpec.flag.set(new AtomicBoolean(false));
        HookTestSpec.beforeSpec.set(new AtomicBoolean(false));
        HookTestSpec.afterSpec.set(new AtomicBoolean(false));
        HookTestSpec.beforeFeature.set(new AtomicBoolean(false));
        HookTestSpec.afterFeature.set(new AtomicBoolean(false));
        HookTestSpec.beforeScenario.set(new AtomicBoolean(false));
        HookTestSpec.afterScenario.set(new AtomicBoolean(false));
        HookTestSpec.noParamBeforeSpec.set(new AtomicBoolean(false));
        HookTestSpec.noParamAfterSpec.set(new AtomicBoolean(false));
        HookTestSpec.noParamBeforeFeature.set(new AtomicBoolean(false));
        HookTestSpec.noParamAfterFeature.set(new AtomicBoolean(false));
        HookTestSpec.noParamBeforeScenario.set(new AtomicBoolean(false));
        HookTestSpec.noParamAfterScenario.set(new AtomicBoolean(false));
    }

    @Test
    public void should_before_spec_method_run_before_specification_execution() {
        final Result result = executeAnnotationHookSpec();
        assertTrue(result.wasSuccessful());
        assertTrue(HookTestSpec.flag.get().get());
        assertTrue(HookTestSpec.beforeSpec.get().get());
    }

    @Test
    public void should_after_spec_method_run_after_specification_execution() {
        final Result result = executeAnnotationHookSpec();
        assertTrue(result.wasSuccessful());
        assertTrue(HookTestSpec.flag.get().get());
        assertTrue(HookTestSpec.afterSpec.get().get());
    }

    @Test
    public void should_before_feature_method_run_before_each_feature_execution() {
        final Result result = executeAnnotationHookSpec();
        assertTrue(result.wasSuccessful());
        assertTrue(HookTestSpec.flag.get().get());
        assertTrue(HookTestSpec.beforeFeature.get().get());
    }

    @Test
    public void should_after_feature_method_run_after_each_feature_execution() {
        final Result result = executeAnnotationHookSpec();
        assertTrue(result.wasSuccessful());
        assertTrue(HookTestSpec.flag.get().get());
        assertTrue(HookTestSpec.afterFeature.get().get());
    }

    @Test
    public void should_before_scenario_method_run_before_each_scenario_execution() {
        final Result result = executeAnnotationHookSpec();
        assertTrue(result.wasSuccessful());
        assertTrue(HookTestSpec.flag.get().get());
        assertTrue(HookTestSpec.beforeScenario.get().get());
    }

    @Test
    public void should_after_scenario_method_run_after_each_scenario_execution() {
        final Result result = executeAnnotationHookSpec();
        assertTrue(result.wasSuccessful());
        assertTrue(HookTestSpec.flag.get().get());
        assertTrue(HookTestSpec.afterScenario.get().get());
    }

    @Test
    public void parameterless_lifecycle_method_should_be_executed() {
        final Result result = executeAnnotationHookSpec();
        assertTrue(result.wasSuccessful());
        assertTrue(HookTestSpec.noParamBeforeSpec.get().get());
        assertTrue(HookTestSpec.noParamAfterSpec.get().get());
        assertTrue(HookTestSpec.noParamBeforeFeature.get().get());
        assertTrue(HookTestSpec.noParamAfterFeature.get().get());
        assertTrue(HookTestSpec.noParamBeforeScenario.get().get());
        assertTrue(HookTestSpec.noParamAfterScenario.get().get());
    }

    private Result executeAnnotationHookSpec() {
        return new JUnitCore().run(new Class[] {HookTestSpec.class});
    }

}
