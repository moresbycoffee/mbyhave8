/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.acceptance;

import lombok.Data;
import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.guice.annotations.FeatureScoped;
import javax.inject.Inject;

import static org.junit.Assert.assertEquals;

/**
 * Tests around {@link org.moresbycoffee.mbyhave8.guice.annotations.FeatureScoped} bean injection.
 */
public class GuiceFeatureScopeTest extends MByHaveSpec {
    {
        Feature("this is a feature in the runner",
                Scenario("in the first feature",
                        given("a first step", this::count)));
        Feature("this is the second feature",
                Scenario("in the second feature",
                        given("and a second step", this::count),
                        then("the counter should be increased only with 2", this::assertFeatureGeneratedTwice)
        ));
    }

    private int counter = 0;

    @Inject
    private FeatureBean featureBean;

    @Data @FeatureScoped public static class FeatureBean {
        private int number = 1;
    }

    void count() {
        int i = featureBean.getNumber();
        counter += i;
        featureBean.setNumber(i + 1);
    }


    void assertFeatureGeneratedTwice() {
        assertEquals(2, featureBean.getNumber());
        assertEquals(2, counter);
    }
}
