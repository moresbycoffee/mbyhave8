/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.annotations;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Stream;

public class AnnotationHookProcess {

    public static HookAnnotations processClass(final Class<?> clazz) {
        final Method[] beforeSpecMethods      = declaredMethods(clazz).filter(AnnotationHookProcess::isBeforeSpec).toArray(Method[]::new);
        final Method[] afterSpecMethods       = declaredMethods(clazz).filter(AnnotationHookProcess::isAfterSpec).toArray(Method[]::new);

        final Method[] beforeFeatureMethods   = declaredMethods(clazz).filter(AnnotationHookProcess::isBeforeFeature).toArray(Method[]::new);
        final Method[] afterFeatureMethods    = declaredMethods(clazz).filter(AnnotationHookProcess::isAfterFeature).toArray(Method[]::new);

        final Method[] beforeScenarionMethods = declaredMethods(clazz).filter(AnnotationHookProcess::isBeforeScenario).toArray(Method[]::new);
        final Method[] afterScenarioMethods   = declaredMethods(clazz).filter(AnnotationHookProcess::isAfterScenario).toArray(Method[]::new);

        return new HookAnnotations(beforeSpecMethods, afterSpecMethods, beforeFeatureMethods, afterFeatureMethods, beforeScenarionMethods, afterScenarioMethods);
    }

    private static Stream<Method> declaredMethods(final Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredMethods());
    }

    private static boolean isBeforeSpec(final Method method) {
        return method.getAnnotation(BeforeSpec.class) != null;
    }

    private static boolean isAfterSpec(final Method method) {
        return method.getAnnotation(AfterSpec.class) != null;
    }

    private static boolean isBeforeFeature(final Method method) {
        return method.getAnnotation(BeforeFeature.class) != null;
    }

    private static boolean isAfterFeature(final Method method) {
        return method.getAnnotation(AfterFeature.class) != null;
    }

    private static boolean isBeforeScenario(final Method method) {
        return method.getAnnotation(BeforeScenario.class) != null;
    }

    private static boolean isAfterScenario(final Method method) {
        return method.getAnnotation(AfterScenario.class) != null;
    }

}
