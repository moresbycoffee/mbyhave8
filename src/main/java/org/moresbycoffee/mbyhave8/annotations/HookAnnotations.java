/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.annotations;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.moresbycoffee.mbyhave8.error.LifeCycleHookException;
import org.moresbycoffee.mbyhave8.hooks.BaseMByHave8Hooks;
import org.moresbycoffee.mbyhave8.hooks.MByHave8Hooks;
import org.moresbycoffee.mbyhave8.result.FeatureResult;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;
import org.moresbycoffee.mbyhave8.structure.Specification;
import org.moresbycoffee.mbyhave8.utils.ReflectionUtils;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Holds the methods annotated with lifeCycle (before and after)
 * annotations.
 */
@Getter
@RequiredArgsConstructor
public class HookAnnotations {

    private final Method[] beforeSpecMethods;
    private final Method[] afterSpecMethods;

    private final Method[] beforeFeatureMethods;
    private final Method[] afterFeatureMethods;

    private final Method[] beforeScenarioMethods;
    private final Method[] afterScenarioMethods;


    public MByHave8Hooks toMByHaveHook(final Object object) {
        return new BaseMByHave8Hooks() {

            @Override
            public void startSpecification(final Specification specification) {
                executeHooks(beforeSpecMethods, object, specification);
            }

            @Override
            public void endSpecification(final Specification specification, final SpecOutput output) {
                executeHooks(afterSpecMethods, object, specification, output);
            }

            @Override
            public void startFeature(final Feature feature) {
                executeHooks(beforeFeatureMethods, object, feature);
            }

            @Override
            public void endFeature(final Feature feature, final FeatureResult result) {
                executeHooks(afterFeatureMethods, object, feature, result);
            }

            @Override
            public void startScenario(final String featureId, final Scenario scenario) {
                executeHooks(beforeScenarioMethods, object, scenario);
            }

            @Override
            public void endScenario(final String featureId, final Scenario scenario,
                    final ScenarioResult result) {
                executeHooks(afterScenarioMethods, object, scenario, result);
            }


        };
    }

    interface InvokeMethodFunction {
        Object apply(final Method method, final Object object) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException;
    }

    private static void executeHooks(final Method[] methods, final Object object, final Object... parameters) {
        for (final Method method : methods) {
            try {
                ReflectionUtils.invokeWithOptionalParameter(method, object, parameters);
            } catch (IllegalAccessException | IllegalArgumentException e) {
                throw new LifeCycleHookException("The '" + method.toString() + "' hook method can not be invoked on instance: " + object, e);
            } catch (final InvocationTargetException e) {
                throw new LifeCycleHookException("Exception occured during execution of " + method.toString(), e.getCause());
            }
        }

    }

}
