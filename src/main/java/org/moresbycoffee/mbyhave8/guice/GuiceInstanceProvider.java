package org.moresbycoffee.mbyhave8.guice;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.moresbycoffee.mbyhave8.InstanceProvider;
import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.guice.annotations.FeatureScoped;
import org.moresbycoffee.mbyhave8.guice.annotations.GuiceInjection;
import org.moresbycoffee.mbyhave8.guice.annotations.ScenarioScoped;
import org.moresbycoffee.mbyhave8.guice.annotations.StepScoped;
import org.moresbycoffee.mbyhave8.hooks.BaseMByHave8Hooks;
import org.moresbycoffee.mbyhave8.hooks.MByHave8Hooks;
import org.moresbycoffee.mbyhave8.result.FeatureResult;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.result.StepResult;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;
import org.moresbycoffee.mbyhave8.structure.Step;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

import lombok.extern.java.Log;

/**
 * Created by envagyok on 25/03/14.
 */
@Log
public class GuiceInstanceProvider implements InstanceProvider {

    @SuppressWarnings("unchecked")
    @Override
    public <T extends MByHaveSpec> T getInstance(final Class<T> testClass) {
        final ProxiedScope featureScope = new ProxiedScope();
        final ProxiedScope scenarioScope = new ProxiedScope();
        final ProxiedScope stepScope = new ProxiedScope();
        final MByHave8Hooks guiceHooks = new BaseMByHave8Hooks() {
            @Override
            public void startFeature(final Feature feature) {
                featureScope.enter();
            }
            @Override
            public void endFeature(final Feature feature, final FeatureResult result) {
                featureScope.exit();
            }
            @Override
            public void startScenario(final String featureId, final Scenario scenario) {
                scenarioScope.enter();
            }
            @Override
            public void endScenario(final String featureId, final Scenario scenario, final ScenarioResult result) {
                scenarioScope.exit();
            }
            @Override
            public void startStep(final String featureId, final String scenarioId, final Step step) {
                stepScope.enter();
            }
            @Override
            public void endStep(final String featureId, final String scenarioId, final Step step, final StepResult result) {
                stepScope.exit();
            }
        };

        final List<Module> userModules = getUserModules(testClass);

        final List<Module> modules = new ArrayList<>();
        modules.add(new AbstractModule() {
            @Override
            protected void configure() {
                // tell GuiceInjection about the scope
                bindScope(FeatureScoped.class, featureScope);
                bindScope(ScenarioScoped.class, scenarioScope);
                bindScope(StepScoped.class, stepScope);

                bind(MByHaveSpec.class).to(testClass);
            }
        });
        modules.addAll(userModules);

        final Injector injector = Guice.createInjector(modules);
        final MByHaveSpec testInstance; //(MByHaveSpec) testClass.newInstance();
        try {
            testInstance = injector.getInstance(MByHaveSpec.class);
            testInstance.registerHooks(guiceHooks);
        } catch (final RuntimeException | Error e) {
            log.log(Level.SEVERE, "Couldn't create specification instance");
            log.log(Level.SEVERE, "Couldn't create specification instance", e);
            throw e;
        }

        return (T) testInstance;
    }

    @SuppressWarnings("unchecked")
    private <T extends MByHaveSpec> List<Module> getUserModules(final Class<T> testClass) {
        final GuiceInjection guiceAnnotation = testClass.getAnnotation(GuiceInjection.class);
        final Class<? extends Module>[] userModuleClasses = guiceAnnotation == null ? null : guiceAnnotation.value();

        return Arrays.asList(userModuleClasses == null ? new Class[]{} : userModuleClasses).stream().map(userModuleClass -> {
            try {
                return (Module) userModuleClass.getConstructor().newInstance();
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
    }
}
