/*
* Moresby Coffee Bean
*
* Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the Moresby Coffee nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package org.moresbycoffee.mbyhave8.guice;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.InvocationHandler;

import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.Scope;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.ScopeHelper;

import lombok.extern.java.Log;


/**
 * Scope what generates proxy object.
 * It will generate the backing object only when the first method invocation happens.
 * The ProxiedScope provides {@link #enter()} and {@link #exit()} methods to support
 * the scope lifecycle.
 *
 * <strong>WARNING:</strong> This implementation uses threads to store the backing values.
 * The scope is entered and exited on each thread separately.
 */
@Log
public class ProxiedScope implements Scope {

    /**
     * Proxy store - all the generated proxies are stored in this map.
     * The map is never emptied only the backing objects are removed.
     */
    private final Map<Key<?>, Object> proxies = new HashMap<>();
    /**
     * {@link java.lang.ThreadLocal} store for the proxy backing objects.
     */
    private final ThreadLocal<Map<Key<?>, Object>> threadLocalValues = new ThreadLocal<>();

    /**
     * This method should be invoked when the scope is available. This method opens up
     * the backing object store.
     */
    public void enter() {
        threadLocalValues.set(new HashMap<>());
    }

    /**
     * This method should be invoked when the scope is no longer available.
     */
    public void exit() {
        threadLocalValues.remove();
        alreadyRequested.remove();
    }

    private final ThreadLocal<Map<Key<?>, Boolean>> alreadyRequested = ThreadLocal.withInitial(() -> new HashMap<Key<?>, Boolean>());

    @Override
    public <T> Provider<T> scope(final Key<T> key, final Provider<T> unscoped) {
        return new Provider<T>() {
            @Override
            @SuppressWarnings("unchecked")
            public T get() {

                if (proxies.containsKey(key)) {
                    if (alreadyRequested.get().containsKey(key) && alreadyRequested.get().get(key)) {
                        //TODO this is a hack. Make it better!
                        final T unscopedInstance = unscoped.get();
                        return unscopedInstance;
                    } else {
                        return (T) proxies.get(key);
                    }
                } else {
                    final T proxy = createProxy(key, unscoped);
                    proxies.put(key, proxy);
                    return proxy;
                }
            }
        };
    }

    /** Unscoped Provider is needed to extract Injector... */
    private <T> T createProxy(final Key<T> key, final Provider<T> unscoped) {
        try {
            @SuppressWarnings("unchecked")
            final T proxy = (T) Enhancer.create(key.getTypeLiteral().getRawType(), new InvocationHandler() {
                @Override
                public Object invoke(final Object o, final Method method, final Object[] objects) throws Throwable {
                    try {
                        return method.invoke(getObject(), objects);
                    } catch (final InvocationTargetException e) {
                        throw e.getCause();
                    }
                }

                T getObject() throws IllegalAccessException, NoSuchFieldException, ErrorsException {
                    final Map<Key<?>, Object> values = threadLocalValues.get();
                    if (values == null) {
                        throw new RuntimeException("Not in feature scope");
                    }
                    if (values.containsKey(key)) {
                        return (T) values.get(key);
                    }
                    alreadyRequested.get().put(key, true);
                    final T newObject = ScopeHelper.getWorkingProvider(unscoped, key).get();
                    values.put(key, newObject);
                    alreadyRequested.get().put(key, false);
                    return newObject;
                }
            });
            return proxy;
        } catch (final RuntimeException | Error e) {
            log.log(Level.SEVERE, "Coult not construct proxy for " + key + " type", e);
            log.fine("Tip: The proxied class needs to have a default constructure or create an interface an try to inject that interface.");
            throw e;
        }

    }


}