/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.utils;

import static java.util.Arrays.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import lombok.NonNull;

/**
 * This utility class contains tools helping dealing with reflection.
 */
public final class ReflectionUtils {


    /**
     * Tries to match the <tt>parameters</tt> with input the <tt>method</tt> input parameters.
     * If it can not match the parameters it tries to call the method with all parameters.
     *
     * @param method The method to be invoked. (NonNull)
     * @param object The object the method should be invoked on. (NonNull)
     * @param parameters The optional parameters. (Nullable)
     * @return The result of the invocation. (Nullable)
     *
     * @throws InvocationTargetException {@link Method#invoke(Object, Object...)}
     * @throws IllegalArgumentException {@link Method#invoke(Object, Object...)}
     * @throws IllegalAccessException {@link Method#invoke(Object, Object...)}
     */
    public static Object invokeWithOptionalParameter(@NonNull final Method method, @NonNull final Object object, final Object... parameters) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {

        final Parameter[] methodParameters = method.getParameters();
        final Object[] inputParameters = getInputParameters(methodParameters, parameters);

        return method.invoke(object, inputParameters);
    }

    /**
     * Tries to match method parameters to the given parameters.
     *
     * @param methodParameters The parameters of the method should be invoked.
     * @param parameters The parameter list we have.
     * @return The parameters can be passed to method.
     * @throws IllegalArgumentException If there is no matching parameter to some methodParameter.
     */
    private static Object[] getInputParameters(final Parameter[] methodParameters, final Object... parameters) throws IllegalArgumentException {
        return stream(methodParameters).map(
                methodParameter -> stream(parameters)
                        .filter(p -> methodParameter.getType().isInstance(p))
                        .findFirst()
                        .<IllegalArgumentException>orElseThrow(() -> new IllegalArgumentException("No matching parameter found for: "+ methodParameter))
                ).toArray();
    }

    /** Hidden constructor of utility class. */
    public ReflectionUtils() { /* NOP */ }

}
