/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.utils;

import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * This class holds static utility/shortcut functions for
 * {@link java.util.Collection}s. These functions easily
 * can be used with
 * <a href="http://projectlombok.org/">Lombok's</a>
 * <a href="http://projectlombok.org/features/experimental/ExtensionMethod.html">ExtensionMethod</a>
 */
public final class ListExtensions {

    /**
     * Map function shortcut for Lists. The usage of the map function removes the
     * <tt>.stream()</tt> and <tt>.collect(Collectors.toList())</tt> method invocations.
     *
     * @param input The input list to be mapped. (NonNull)
     * @param mapper The mapper function.
     * @param <F> The type of the input list.
     * @param <T> The type of the output list.
     * @return The output list mapped from the <tt>input</tt> list by the <tt>mapper</tt> function.
     */
    public static <F, T> List<T> map(final List<? extends F> input, final Function<? super F, ? extends T> mapper) {
        return input.stream().map(mapper).collect(Collectors.toList());
    }

    /**
     * Simple right folding operation for {@link java.util.Collection}s.
     *
     * @param input The original {@link java.util.Collection} to be folded. (NonNull)
     * @param identity The initial value (identity) for the folding operation.
     * @param folder The folding function taking the previous result and the next item.
     * @param <T> The type of the input
     * @param <U> The type of the output
     * @return The result of the folding operation.
     */
    public static <T, U> U fold(final Collection<? extends T> input, final U identity, final BiFunction<? super U, ? super T, ? extends U> folder) {
        U result = identity;
        for (final T item : input) {
            result = folder.apply(result, item);
        }
        return result;

    }




    /** Hidden constructor of utility class*/
    private ListExtensions() { /* NOP*/ }

    public static <T> T head(final List<T> list) {
        if (list == null) {
            throw new NullPointerException("Head of the null list is unresolvable");
        }
        return list.isEmpty() ? null : list.iterator().next();
    }
}
