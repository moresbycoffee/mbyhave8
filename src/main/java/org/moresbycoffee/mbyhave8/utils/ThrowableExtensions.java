/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Utility methods to handle {@link java.lang.Throwable}s/{@link java.lang.Exception}s.
 * These functions easily
 * can be used with
 * <a href="http://projectlombok.org/">Lombok's</a>
 * <a href="http://projectlombok.org/features/experimental/ExtensionMethod.html">ExtensionMethod</a>
 */
public class ThrowableExtensions {

    public static void writeTo(final Throwable error, final Writer writer, final int numOfElements) {
        final PrintWriter errorWriter = new PrintWriter(writer, true);
        errorWriter.println(error);

        final StackTraceElement[] trace = error.getStackTrace();
        for (int i = 0; i < trace.length && i < numOfElements; i++) {
            StackTraceElement traceElement = trace[i];
            errorWriter.println("\tat " + traceElement);
        }
    }

    public static String convertToString(final Throwable error, final int numOfElements) {

        final StringWriter errorStringWriter = new StringWriter();
        writeTo(error, errorStringWriter, numOfElements);
        errorStringWriter.flush();

        return errorStringWriter.toString();
    }

    /**
     * Turns an error to a string with the full stackTrace.
     * @param error The error
     * @return The stackTrace in a string.
     */
    public static String convertToString(Throwable error) {
        StringWriter sw = new StringWriter();
        PrintWriter a = new PrintWriter(sw, true);
        error.printStackTrace(a);

        sw.flush();
        return sw.toString();
    }

    /** Hidden constructor of utility class. */
    private ThrowableExtensions() { /* NOP*/ }
}
