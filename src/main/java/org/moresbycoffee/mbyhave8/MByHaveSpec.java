/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8;

import java.io.PrintWriter;
import java.io.Writer;

import org.junit.runner.RunWith;
import org.moresbycoffee.mbyhave8.filter.Filter;
import org.moresbycoffee.mbyhave8.hooks.CallbackAnnouncer;
import org.moresbycoffee.mbyhave8.hooks.MByHave8Hooks;
import org.moresbycoffee.mbyhave8.junit.MByHave8Runner;
import org.moresbycoffee.mbyhave8.report.AnsiWriterReporter;
import org.moresbycoffee.mbyhave8.report.Reporter;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.result.StepResult;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;
import org.moresbycoffee.mbyhave8.structure.Specification;
import org.moresbycoffee.mbyhave8.structure.Step;

import lombok.Getter;

/**
 * The main class of MByHave8. This class holds the whole specification.
 */
@RunWith(MByHave8Runner.class)
public abstract class MByHaveSpec {

    @Getter
    private Specification specification = new Specification();
    //TODO replace announcer
    private final CallbackAnnouncer eventAnnouncer = new CallbackAnnouncer();

    protected final Feature Feature(final String description, final Scenario... scenarios) {
        final Feature feature = new Feature(description, scenarios);
        specification = specification.withAdditionalFeature(feature);
        return feature;
    }

    protected final Scenario Scenario(final String description, final Step... steps) {
        return new Scenario(description, steps);
    }


    protected final Step given(final String description, final StepImplementation stepImplementation) {
        return new Step("Given", description, stepImplementation);
    }

    protected final Step given(final String description, final VoidStepImplementation stepImplementation) {
        return given(description, voidToNormalStepImplementation(stepImplementation));
    }

    protected final Step given(final String description) {
        return given(description, (VoidStepImplementation) () -> { throw new PendingException("Step is not implemented yet."); });
    }

    protected final Step when(final String description, final StepImplementation stepImplementation) {
        return new Step("When", description, stepImplementation);
    }

    protected final Step when(final String description, final VoidStepImplementation stepImplementation) {
        return when(description, voidToNormalStepImplementation(stepImplementation));
    }

    protected final Step when(final String description) {
        return when(description, (VoidStepImplementation) () -> { throw new PendingException("Step is not implemented yet."); });
    }

    protected final Step then(final String description, final StepImplementation stepImplementation) {
        return new Step("Then", description, stepImplementation);
    }

    protected final Step then(final String description, final VoidStepImplementation stepImplementation) {
        return then(description, voidToNormalStepImplementation(stepImplementation));
    }

    protected final Step then(final String description) {
        return then(description, (VoidStepImplementation) () -> { throw new PendingException("Step is not implemented yet."); });
    }

    protected final Step and(final String description, final StepImplementation stepImplementation) {
        return new Step("And", description, stepImplementation);
    }

    protected final Step and(final String description, final VoidStepImplementation stepImplementation) {
        return and(description, voidToNormalStepImplementation(stepImplementation));
    }

    protected final Step and(final String description) {
        return and(description, (VoidStepImplementation) () -> { throw new PendingException("Step is not implemented yet."); });
    }

    public final void registerHooks(final MByHave8Hooks hooks) {
        this.eventAnnouncer.addListener(hooks);
    }

    private StepImplementation voidToNormalStepImplementation(final VoidStepImplementation voidStepImplementation) {
        return new StepImplementation() {
            @Override
            public StepResult step() {
                try {
                    voidStepImplementation.step();
                    return StepResult.Success;
                } catch (final AssertionError assertionError) {
                    return StepResult.failure(assertionError);
                } catch (final PendingException pendingException) {
                    return StepResult.Pending;
                } catch (final Throwable t) {
                    return StepResult.error(t);
                }
            }
        };
    }

    public final SpecOutput execute() {
        final PrintWriter outputWriter = new PrintWriter(System.out);
        final SpecOutput output = execute(outputWriter);
        outputWriter.flush();
        return output;
    }

    public final SpecOutput execute(final Writer writer) {
        return execute(new AnsiWriterReporter(writer));
    }

    public final SpecOutput execute(final Reporter reporter) {
        final Filter filter = new Filter(System.getProperty("mbyhave.tags"));
        return specification.execute(this.getClass(), reporter, eventAnnouncer, filter);
    }


}
