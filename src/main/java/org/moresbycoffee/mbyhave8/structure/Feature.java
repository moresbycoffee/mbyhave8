/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.structure;

import static org.moresbycoffee.mbyhave8.utils.TagUtils.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import org.moresbycoffee.mbyhave8.filter.Filter;
import org.moresbycoffee.mbyhave8.hooks.FeatureAwareScenarioHook;
import org.moresbycoffee.mbyhave8.hooks.MByHave8Hooks;
import org.moresbycoffee.mbyhave8.hooks.ScenarioHooks;
import org.moresbycoffee.mbyhave8.result.FeatureOutput;
import org.moresbycoffee.mbyhave8.result.FeatureResult;
import org.moresbycoffee.mbyhave8.result.ScenarioOutput;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.utils.ListExtensions;

import lombok.Getter;

/**
 * Represent a Feature in the {@link Specification}. The Feature consists of
 * zero, one or multiple {@link Scenario}s. If there is no {@link Scenario}
 * added to the Feature, the feature is in {@link FeatureResult#Pending Pending}
 * state, otherwise the Feature's state is determined by the {@link Scenario}s.
 */
@Getter
public class Feature {

    /** Unique identifier (UUID) of the feature. (NonNull) */
    private final String id = UUID.randomUUID().toString();
    /** Human readable freetext description of the feature. (NonNull) */
    private final String description;
    /** Immutable list of {@link Scenario}s in the the Feature. */
    private final List<Scenario> scenarios;
    /** The freetext tags on the feature. (NonNull) */
    private final List<String> tags = new ArrayList<>();
    /**
     * List of registered <em>issue id</em>s. (NonNull)<br>
     * Used in error reporting to link the error to the
     * <em>user story</em>, <em>bug report</em>, etc.
     */
    private final List<String> issues = new ArrayList<>();

    public Feature(final String description, final Scenario[] scenarios) {
        this.description = "Feature: " + description;
        this.scenarios = Collections.unmodifiableList(Arrays.asList(scenarios));
    }

    public String getDescription() {
        return description;
    }

    public List<Scenario> getScenarios() {
        return scenarios;
    }

    public boolean isExecutable(final Filter filter) {
        return scenarios.isEmpty() || scenarios.stream().anyMatch(s -> filter.isExecutable(getTagsForFiltering(s)));
    }

    public FeatureOutput execute(final MByHave8Hooks hooks, final Filter filter) {
        return executeHelper(Scenario::execute, hooks, filter);
    }

    public FeatureOutput executeHelper(final BiFunction<Scenario, ScenarioHooks, ScenarioOutput> scenarioExecutor, final MByHave8Hooks hooks, final Filter filter) {
        final ScenarioHooks scenarioHooks = new FeatureAwareScenarioHook(id, hooks);

        final FeatureResult result;
        final List<ScenarioOutput> scenarioOutputs;
        if (scenarios == null || scenarios.isEmpty()) {
            result = FeatureResult.Pending;
            scenarioOutputs = Collections.emptyList();
        } else {
            hooks.startFeature(this);
            scenarioOutputs = scenarios.stream()
                    .filter(s -> filter.isExecutable(getTagsForFiltering(s)))
                    .map(scenario -> scenarioExecutor.apply(scenario, scenarioHooks))
                    .collect(Collectors.toList());

            result = ListExtensions.fold(scenarioOutputs, FeatureResult.Success, this::folder);
            hooks.endFeature(this, result);
        }

        return FeatureOutput.builder().
                description(getDescription()).
                result(result).
                scenarios(scenarioOutputs).
                issues(Collections.unmodifiableCollection(issues)).
                build();
    }

    private List<String> getTagsForFiltering(final Scenario s) {
        final List<String> tags = new ArrayList<>(getTags());
        tags.addAll(s.getTags());
        return tags;
    }

    private FeatureResult folder(final FeatureResult previousResult, final ScenarioOutput item) {
        final ScenarioResult scenarioResult = item.getResult();
        switch (scenarioResult) {
            case Error: {
                return FeatureResult.Error;
            }
            case Pending: {
                if (EnumSet.of(FeatureResult.Error, FeatureResult.Failure).contains(previousResult)) {
                    return previousResult;
                }
                return FeatureResult.Pending;
            }
            case Failure: {
                if (FeatureResult.Error.equals(previousResult)) {
                    return previousResult;
                }
                return FeatureResult.Failure;
            }
            case Success: {
                return previousResult;
            }
            default : {
                throw new IllegalStateException("The [" + scenarioResult + "] scenario result state is not supported by the Feature processor");
            }
        }
    }

    /**
     * Registers the given <em>tag</em>(s) to the feature. The tags are used in
     * filtering.<br>
     * The tags are {@link String#trim() trimmed} and <tt>null</tt> values are
     * skipped.
     *
     * @param tags The tags to be registered. (Nullable)
     * @return the Feature for further chaining.
     */
    public Feature tag(final String... tags) {
        if (tags != null) {
            this.tags.addAll(cleanTags(tags));
        }
        return this;
    }

    /**
     * Registers the given <em>issue id</em>(s) to the feature. The issue number
     * is used in error reporting as well as in filtering as a <em>tag</em>.<br>
     * The <em>issue ids</em> are {@link String#trim() trimmed} and
     * <tt>null</tt> values are skipped.
     *
     * @param issues The issue id(s)
     * @return the Feature for further chaining.
     */
    public Feature issue(final String... issues) {
        if (issues != null) {
            final List<String> issueList = cleanTags(issues);
            this.issues.addAll(issueList);
            this.tags.addAll(issueList);
        }
        return this;
    }


}
