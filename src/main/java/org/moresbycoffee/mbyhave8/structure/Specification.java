/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.structure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.moresbycoffee.mbyhave8.filter.Filter;
import org.moresbycoffee.mbyhave8.hooks.MByHave8Hooks;
import org.moresbycoffee.mbyhave8.report.Reporter;
import org.moresbycoffee.mbyhave8.result.FeatureOutput;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.result.SpecResult;
import org.moresbycoffee.mbyhave8.utils.ListExtensions;

import lombok.Getter;

/**
 * Represents a Specification with the Features in it.
 */
public class Specification {

    @Getter
    private final List<Feature> features;
    @Getter
    private final String id = UUID.randomUUID().toString();

    public Specification(final Feature... features) {
        if (features == null || features.length == 0) {
            this.features = Collections.emptyList();
        } else {
            this.features = Collections.unmodifiableList(new ArrayList<Feature>(Arrays.asList(features)));
        }
    }

    public Specification(final Collection<Feature> features, final Feature... newFeatures) {
        final List<Feature> mutableFeatureList = new ArrayList<>();
        if (features != null && !features.isEmpty()) {
            mutableFeatureList.addAll(features);
        }
        if (newFeatures != null && newFeatures.length != 0) {
            mutableFeatureList.addAll(Arrays.asList(newFeatures));
        }
        this.features = Collections.unmodifiableList(mutableFeatureList);
    }

    public Specification withAdditionalFeature(final Feature... newFeatures) {
        return new Specification(this.features, newFeatures);
    }



    /**
     * Returns true when the Specification doesn't contain any feature or there
     * is at least one feature what is executable with the given <tt>filter</tt>.
     *
     * @param filter The filter
     * @return <tt>true</tt> if the Specification contains executable {@link Feature}.
     */
    public boolean isExecuteable(final Filter filter) {
        return features.isEmpty() || features.stream().anyMatch(feature -> feature.isExecutable(filter));
    }


    public final SpecOutput execute(final Class<?> testClass, final Reporter reporter, final MByHave8Hooks hooks, final Filter filter) {
        final SpecOutput output = executeHelper(testClass, Feature::execute, hooks, filter);
        reporter.report(output);
        return output;
    }

    private SpecOutput executeHelper(final Class<?> testClass, final TriFunction<Feature, MByHave8Hooks, Filter, FeatureOutput> featureExecutor, final MByHave8Hooks hooks, final Filter filter) {
        if (this.isExecuteable(filter)) {
            hooks.startSpecification(this);
            final Collection<FeatureOutput> featureOutputs = features.stream()
                    .filter(feature -> feature.isExecutable(filter))
                    .map(feature -> featureExecutor.apply(feature, hooks, filter))
                    .collect(Collectors.toList());
            final SpecResult result = ListExtensions.fold(featureOutputs, SpecResult.Success, (previousResult, item) -> previousResult.addFeatureResult(item.getResult()));
            final SpecOutput output = new SpecOutput(testClass.getName(), result, featureOutputs);
            hooks.endSpecification(this, output);
            return output;
        } else {
            return new SpecOutput(testClass.getName(), SpecResult.Success, Collections.emptyList());
        }

    }

    @FunctionalInterface
    public interface TriFunction<T, U, V, R> {

        /**
         * Applies this function to the given arguments.
         *
         * @param t the first function argument
         * @param u the second function argument
         * @param v the third function argument
         * @return the function result
         */
        R apply(final T t, final U u, final V v);
    }


}
