/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.structure;

import java.util.UUID;

import org.moresbycoffee.mbyhave8.StepImplementation;
import org.moresbycoffee.mbyhave8.hooks.StepHooks;
import org.moresbycoffee.mbyhave8.result.StepOutput;
import org.moresbycoffee.mbyhave8.result.StepResult;

import lombok.Getter;
/**
 * Created with IntelliJ IDEA.
 * User: envagyok
 * Date: 30/11/13
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
@Getter
public class Step {

    private final String id = UUID.randomUUID().toString();
    private final String stepName;
    private final String description;
    private final StepImplementation stepImplementation;

    public Step(final String stepName, final String description, final StepImplementation stepImplementation) {
        this.stepName = stepName;
        this.description = description;
        this.stepImplementation = stepImplementation;
    }

    public String getDescription() {
        return stepName + " " + description;
    }

    public StepOutput execute(final StepHooks hooks) {

        hooks.startStep(this);

        StepResult result;
        try {
            result = stepImplementation.step();
        } catch (final Throwable t) {
            t.printStackTrace(); //TODO replace with some logger
            result = StepResult.error(t);
        }
        hooks.endStep(this, result);
        return new StepOutput(getDescription(), result);
    }

    public StepOutput skip(final StepHooks hooks) {
        hooks.startStep(this);
        hooks.endStep(this, StepResult.Skipped);
        return new StepOutput(getDescription(), StepResult.Skipped);
    }

}
