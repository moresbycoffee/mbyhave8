/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.structure;

import static org.moresbycoffee.mbyhave8.utils.TagUtils.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.function.BiFunction;

import org.moresbycoffee.mbyhave8.hooks.ScenarioAwareStepHook;
import org.moresbycoffee.mbyhave8.hooks.ScenarioHooks;
import org.moresbycoffee.mbyhave8.hooks.StepHooks;
import org.moresbycoffee.mbyhave8.result.ScenarioOutput;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.result.StepOutput;
import org.moresbycoffee.mbyhave8.result.StepResult;

import lombok.Getter;

/**
 * Represents a scenario.
 * TODO make it immutable
 */
@Getter
public class Scenario {

    /** Unique identifier (UUID) of the scenario. (NonNull) */
    private final String       id;
    /** Human readable freetext description of the scenario. (NonNull) */
    private final String       description;
    /** The steps of the scenario. Immutable list. (NonNull) */
    private final List<Step>   steps;
    /** The freetext tags on the scenario. (NonNull) */
    private final List<String> tags = new ArrayList<>();
    /**
     * List of registered <em>issue id</em>s. (NonNull)<br>
     * Used in error reporting to link the error to the
     * <em>user story</em>, <em>bug report</em>, etc.
     */
    private final List<String> issues = new ArrayList<>();


    public Scenario(final String description, final Step... steps) {
        this.id          = UUID.randomUUID().toString();
        this.description = "Scenario: " + description;
        this.steps       = Collections.unmodifiableList(Arrays.asList(steps));
    }

    public List<Step> getSteps() {
        return steps;
    }

    public String getDescription() {
        return description;
    }


    public ScenarioOutput execute(final ScenarioHooks hooks) {
        return executeHelper(Step::execute, Step::skip, hooks);
    }

    private ScenarioOutput executeHelper(final BiFunction<Step, StepHooks, StepOutput> stepExecutor, final BiFunction<Step, StepHooks, StepOutput> skipStep, final ScenarioHooks hooks) {
        hooks.startScenario(this);

        final StepHooks stepHooks = new ScenarioAwareStepHook(id, hooks);

        ScenarioResult result = ScenarioResult.Success;
        final List<StepOutput> stepOutputs;
        if (steps.isEmpty()) {
            result = ScenarioResult.Pending;
            stepOutputs = Collections.emptyList();
        } else {
            stepOutputs = new ArrayList<StepOutput>(steps.size());
            for (final Step step : steps) {
                final StepOutput stepOutput;
                if (ScenarioResult.Success.equals(result)) {
                    stepOutput = stepExecutor.apply(step, stepHooks);
                    final StepResult stepResult = stepOutput.getResult();
                    if (stepResult instanceof StepResult.Error) {
                        result = ScenarioResult.Error;
                    } else if (stepResult instanceof StepResult.Failure) {
                        result = ScenarioResult.Failure;
                    } else if (stepResult instanceof StepResult.Pending) {
                        result = ScenarioResult.Pending;
                    } else if (stepResult instanceof StepResult.Skipped) {
                        //Do nothing
                    } else if (stepResult instanceof StepResult.Success) {
                        //Do nothing
                    } else {
                        throw new IllegalStateException("The [" + stepResult+ "] step result is not supported by the Scenario");
                    }
                } else {
                    stepOutput = skipStep.apply(step, stepHooks);
                }
                stepOutputs.add(stepOutput);


            }
        }

        hooks.endScenario(this, result);

        return ScenarioOutput.builder().
                description(getDescription()).
                result(result).
                steps(stepOutputs).
                issues(Collections.unmodifiableList(issues)).
                build();
    }

    /**
     * Registers the given <em>tag</em>(s) to the scenario. The tags are used in
     * filtering.<br>
     * The tags are {@link String#trim() trimmed} and <tt>null</tt> values are
     * skipped.
     *
     * @param tags The tags to be registered. (Nullable)
     * @return the Scenario for further chaining.
     */
    public Scenario tag(final String... tags) {
        if (tags != null) {
            this.tags.addAll(cleanTags(tags));
        }
        return this;
    }

    /**
     * Registers the given <em>issue id</em>(s) to the scenario. The issue
     * number is used in error reporting as well as in filtering as a
     * <em>tag</em>.<br>
     * The <em>issue ids</em> are {@link String#trim() trimmed} and
     * <tt>null</tt> values are skipped.
     *
     * @param issues The issue id(s)
     * @return the Scenario for further chaining.
     */
    public Scenario issue(final String... issues) {
        if (issues != null) {
            final List<String> issueList = cleanTags(issues);
            this.issues.addAll(issueList);
            this.tags.addAll(issueList);
        }
        return this;
    }

}
