/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;

import lombok.NonNull;
import lombok.val;
import lombok.extern.java.Log;

/**
 * Manages the configuration of MByHave8. It reads the configuration from
 * <tt>mbyhave8.properties</tt> file.
 */
@Log
public class Configuration {

    /** The name of the configuration file loaded from classpath. */
    private static final String CONFIG_FILE_NAME = "mbyhave8.properties";
    /** The preloaded configuration file. The config file is loaded only once. */
    private static final Properties configFileProperties = loadConfigurationFile();

    /**
     * Returns the configuration value from config file or system environment
     * parameter. If there are configuration both in config file and system
     * environment the latest will be returned.
     *
     * @param key The key to the configuration. (NonNull)
     * @return The value if available, otherwise <tt>null</tt>. (Nullable)
     */
    public static String getConfig(final @NonNull String key) {
        return getConfig(key, null);
    }

    /**
     * Returns the configuration value from config file or system environment
     * parameter. If there are configuration both in config file and system
     * environment the latest will be returned.
     *
     * @param key
     *            The key to the configuration. (NonNull)
     * @param defaultValue
     *            If there is no value either in config file or system
     *            properties, the method will return <tt>defaultValue</tt>.
     *            (Nullable)
     * @return The value if available, otherwise <tt>null</tt>. (Nullable)
     */
    public static String getConfig(final @NonNull String key, final String defaultValue) {
        val systemPropValue = getSystemProperty(key);
        if (systemPropValue != null) {
            return systemPropValue;
        }
        val configFileValue = getConfigurationFileProperty(key);
        if (configFileValue != null) {
            return configFileValue;
        }
        return defaultValue;
    }

    private static String getConfigurationFileProperty(final String key) {
        val value = configFileProperties.getProperty(key);
        return value == null || value.length() == 0 ? null : value;
    }

    private static Properties loadConfigurationFile() {
        val properties = new Properties();
        try (val propertiesInputStream = Configuration.class.getClassLoader().getResourceAsStream(CONFIG_FILE_NAME)) {
            if (propertiesInputStream != null) {
                properties.load(propertiesInputStream);
            }
        } catch (final IOException e) {
            log.log(Level.WARNING, "MByHave8 config is not in the classpath", e);
        }
        return properties;
    }

    private static String getSystemProperty(final String key) {
        final String value = System.getProperty(key);
        return value == null || value.length() == 0 ? null : value;
    }

    /** Hidden constructor of Utility class. */
    private Configuration() { /* NOP */}

}
