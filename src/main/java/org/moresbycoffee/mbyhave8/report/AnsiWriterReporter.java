/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.report;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;

import org.fusesource.jansi.Ansi;
import org.moresbycoffee.mbyhave8.Configuration;
import org.moresbycoffee.mbyhave8.result.FeatureOutput;
import org.moresbycoffee.mbyhave8.result.ScenarioOutput;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.result.StepOutput;
import org.moresbycoffee.mbyhave8.result.StepResult;
import org.moresbycoffee.mbyhave8.utils.ThrowableExtensions;

import lombok.val;
import lombok.experimental.ExtensionMethod;

/**
 * Created by envagyok on 13/03/14.
 */
@ExtensionMethod(ThrowableExtensions.class)
public class AnsiWriterReporter implements Reporter {

    private final Writer writer;

    public AnsiWriterReporter(final Writer writer) {
        this.writer = writer;
    }

    @Override
    public void report(final SpecOutput output) {
        output.getFeatures().forEach(this::reportFeature);
    }

    public void reportFeature(final FeatureOutput featureOutput) {
        final Ansi.Color color;
        switch (featureOutput.getResult()) {
            case Error: {
                color = RED;
                break;
            }
            case Failure: {
                color = YELLOW;
                break;
            }
            case Pending: {
                color = CYAN;
                break;
            }
            case Success: {
                color = GREEN;
                break;
            }
            default:
                throw new IllegalStateException("The feature result state is not supported by this reporter.");
        }

        try {
            writer.append(ansi().fg(color).a(featureOutput.getDescription()).reset().newline().toString());
        } catch (final IOException e) {
            throw new IllegalStateException(e);
        }

        featureOutput.getScenarios().forEach(sc -> reportScenario(sc, featureOutput.getIssues()));

    }

    public void reportScenario(final ScenarioOutput scenarioOutput, final Collection<String> featureIssues) {
        final Ansi.Color color;
        switch (scenarioOutput.getResult()) {
            case Error: {
                color = RED;
                break;
            }
            case Failure: {
                color = YELLOW;
                break;
            }
            case Pending: {
                color = CYAN;
                break;
            }
            case Success: {
                color = GREEN;
                break;
            }
            default:
                throw new IllegalStateException("The feature result state is not supported by this reporter.");
        }

        try {
            writer.append(ansi().fg(color).a("    " + scenarioOutput.getDescription()).reset().newline().toString());
        } catch (final IOException e) {
            throw new IllegalStateException(e);
        }

        val issues = new ArrayList<String>();
        if (featureIssues != null) {
            issues.addAll(featureIssues);
        }
        val scenarioIssues = scenarioOutput.getIssues();
        if (scenarioIssues != null) {
            issues.addAll(scenarioIssues);
        }
        scenarioOutput.getSteps().forEach(so -> reportStep(so, issues));
    }

    private void reportStep(final StepOutput stepOutput, final Collection<String> issues) {
        final Ansi.Color color;
        final StepResult stepResult = stepOutput.getResult();
        if (stepResult instanceof StepResult.Error) {
            color = RED;
        } else if (stepResult instanceof StepResult.Failure) {
            color = YELLOW;
        } else if (stepResult instanceof StepResult.Pending) {
            color = CYAN;
        } else if (stepResult instanceof StepResult.Skipped) {
            color = MAGENTA;
        } else if (stepResult instanceof StepResult.Success) {
            color = GREEN;
        } else {
            throw new IllegalStateException("The [" + stepResult+ "] step result is not supported by the reporter");
        }

        try {
            final Ansi stepReportOutput = ansi().fg(color).a("        " + stepOutput.getDescription()).reset().newline();
            writer.append(stepReportOutput.toString());
            if (stepResult instanceof StepResult.Error || stepResult instanceof StepResult.Failure) {
                for (final String issue : issues) {
                    final String issueTrackerUrlPattern = Configuration.getConfig("issueTrackerUrlPattern", "%s");
                    writer.append("Issue: " + String.format(issueTrackerUrlPattern, issue) + "\n");
                }
            }

            if (stepResult instanceof StepResult.Error) {
                stepResult.getException().printStackTrace(new PrintWriter(writer, true));
            }
            if (stepResult instanceof StepResult.Failure) {
                stepResult.getException().writeTo(writer, 3);
            }

        } catch (final IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
