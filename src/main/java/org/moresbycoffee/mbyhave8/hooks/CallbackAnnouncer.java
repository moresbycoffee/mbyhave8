/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.hooks;

import java.util.LinkedList;

import org.moresbycoffee.mbyhave8.result.FeatureResult;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.result.StepResult;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;
import org.moresbycoffee.mbyhave8.structure.Specification;
import org.moresbycoffee.mbyhave8.structure.Step;

/**
 * <p>MByHave8 specific callback announcer. This class maintains a
 * list of hooks and it invokes them one-by-one when the given
 * event happens.</p>
 * <p>The <em>start</em> hooks are invoked in the order as they
 * were attached. The <em>end</em> hooks are invoked in a
 * <strong>reverse</strong> order.</p>
 */
public class CallbackAnnouncer implements MByHave8Hooks {

    private final LinkedList<MByHave8Hooks> listeners = new LinkedList<MByHave8Hooks>();

    public void addListener(final MByHave8Hooks hooks) {
        listeners.add(hooks);
    }


    @Override
    public void startSpecification(final Specification specification) {
            listeners.forEach(a -> a.startSpecification(specification));
    }

    @Override
    public void endSpecification(final Specification specification, final SpecOutput output) {
        listeners.descendingIterator().forEachRemaining(a -> a.endSpecification(specification, output));
    }

    @Override
    public void startFeature(final Feature feature) {
        listeners.forEach(a -> a.startFeature(feature));

    }

    @Override
    public void endFeature(final Feature feature, final FeatureResult result) {
        listeners.descendingIterator().forEachRemaining(a -> a.endFeature(feature, result));
    }

    @Override
    public void startScenario(final String featureId, final Scenario scenario) {
        listeners.forEach(a -> a.startScenario(featureId, scenario));
    }

    @Override
    public void endScenario(final String featureId, final Scenario scenario,
            final ScenarioResult result) {
        listeners.descendingIterator().forEachRemaining(a -> a.endScenario(featureId, scenario, result));
    }

    @Override
    public void startStep(final String featureId, final String scenarioId, final Step step) {
        listeners.forEach(a -> a.startStep(featureId, scenarioId, step));

    }

    @Override
    public void endStep(final String featureId, final String scenarioId, final Step step,
            final StepResult result) {
        listeners.descendingIterator().forEachRemaining(a -> a.endStep(featureId, scenarioId, step, result));
    }


}
