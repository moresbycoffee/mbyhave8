/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.result;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created with IntelliJ IDEA.
 * User: envagyok
 * Date: 30/11/13
 * Time: 08:11
 * To change this template use File | Settings | File Templates.
 */
public abstract class StepResult {

    private StepResult() {}

    public Throwable getException() { return null; }

    public static final StepResult Success = new Success();
    public static final StepResult Pending = new Pending();
    public static final StepResult Skipped = new Skipped();

    public static StepResult error(Throwable throwable) {
        return new Error(throwable);
    }

    public static StepResult failure(AssertionError assertionError) {
        return new Failure(assertionError);
    }

    public static StepResult success() {
        return Success;
    }

    public static StepResult pending() {
        return Pending;
    }

    public static StepResult skipped() {
        return Skipped;
    }




    @Getter
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class Error extends StepResult {
        private final Throwable throwable;

        @Override
        public Throwable getException() {
            return throwable;
        }
    }

    @Getter
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class Failure extends StepResult {
        private final AssertionError error;

        @Override
        public Throwable getException() {
            return error;
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class Pending extends StepResult {
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class Success extends StepResult {
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class Skipped extends StepResult {
    }

}
