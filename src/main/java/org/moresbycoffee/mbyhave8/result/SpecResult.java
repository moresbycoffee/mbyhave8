/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.result;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: envagyok
 * Date: 30/11/13
 * Time: 18:05
 * To change this template use File | Settings | File Templates.
 */
public enum SpecResult {
    Error(Collections.<SpecResult>emptySet()),
    Failure(new HashSet<>(Arrays.asList(Error))),
    Pending(new HashSet<>(Arrays.asList(Failure, Error))),
    Success(new HashSet<>(Arrays.asList(Pending, Failure, Error)));

    private final Set<SpecResult> strongerResults;

    private SpecResult(Set<SpecResult> strongerResults) {
        this.strongerResults = strongerResults;
    }

    public SpecResult addFeatureResult(final FeatureResult featureResult) {
        return addResult(mapFeatureResult(featureResult));
    }

    private SpecResult addResult(SpecResult specResult) {
        if (strongerResults.contains(specResult)) {
            return specResult;
        } else {
            return this;
        }
    }

    private SpecResult mapFeatureResult(final FeatureResult featureResult) {
        switch (featureResult) {
            case Error: {
                return SpecResult.Error;
            }
            case Pending: {
                return SpecResult.Pending;
            }
            case Failure: {
                return SpecResult.Failure;
            }
            case Success: {
                return SpecResult.Success;
            }
            default: {
                throw new IllegalArgumentException("Teh Feature result is unknown: " + featureResult);
            }
        }
    }

}
