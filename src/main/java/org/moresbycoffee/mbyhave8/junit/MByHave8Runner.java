/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.junit;

import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import org.moresbycoffee.mbyhave8.InstanceProvider;
import org.moresbycoffee.mbyhave8.MByHaveSpec;
import org.moresbycoffee.mbyhave8.annotations.AnnotationHookProcess;
import org.moresbycoffee.mbyhave8.guice.GuiceInstanceProvider;

/**
 * MByHave8 JUnit runner. This class plugs {@link MByHaveSpec MByHave
 * specification}s into JUnit.
 */
public class MByHave8Runner extends Runner {

    /** The tested class. */
    private final Class<? extends MByHaveSpec> testClass;
    private final InstanceProvider instanceProvider = new GuiceInstanceProvider();
    private MByHaveSpec testInstance;


    @SuppressWarnings("unchecked")
    public MByHave8Runner(final Class<?> testClass) {
        this.testClass = (Class<? extends MByHaveSpec>) testClass;
    }

    private MByHaveSpec getInstance() throws InstantiationException, IllegalAccessException {
        if (testInstance == null) {
            testInstance = instanceProvider.getInstance(testClass);
        }

        return testInstance;
    }

    private JUnitDescription jUnitDescription;

    private JUnitDescription getJUnitDescription() throws IllegalAccessException, InstantiationException {
        if (jUnitDescription == null) {
            jUnitDescription = JUnitDescription.parse(getInstance().getSpecification(), testClass);
        }
        return jUnitDescription;
    }

    @Override
    public Description getDescription() {
        try {
            return getJUnitDescription().getRootDescription();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run(final RunNotifier runNotifier) {
        try {

            getInstance().registerHooks(new JUnitNotifier(runNotifier, getJUnitDescription()));
            getInstance().registerHooks(AnnotationHookProcess.processClass(testClass).toMByHaveHook(testClass.cast(getInstance())));
            getInstance().execute();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
