/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.junit;

import java.util.HashMap;
import java.util.List;

import org.junit.runner.Description;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;
import org.moresbycoffee.mbyhave8.structure.Specification;
import org.moresbycoffee.mbyhave8.structure.Step;

import lombok.Getter;

/**
 * TODO javadoc.
 * TODO this should be immutable, but it's really difficult to do so.
 */
public class JUnitDescription extends HashMap<String, Description> {

    /** @see java.io.Serializable */
    private static final long serialVersionUID = 1L;

    public static JUnitDescription parse(final Specification specification, final Class<?> testClass) {
        return new JUnitDescription(specification, testClass);
    }

    @Getter
    private final Description rootDescription;

    private JUnitDescription(final Specification specification, final Class<?> testClass) {
        this.rootDescription = createJUnitDescription(specification, testClass);
    }

    private Description createJUnitDescription(final Specification specification, final Class<?> testClass) {
        final Description mainDescription   = Description.createSuiteDescription(testClass);
        specification.getFeatures().forEach(feature -> mainDescription.addChild(createJUnitDescription(feature, testClass)));
        this.put(specification.getId(), mainDescription);
        return mainDescription;
    }

    private Description createJUnitDescription(final Feature feature, final Class<?> testClass) {
        final String description = feature.getDescription();
        final List<Scenario> scenarios = feature.getScenarios();
        final Description featureDescription = Description.createSuiteDescription(description);
        scenarios.forEach(scenario -> featureDescription.addChild(createJUnitDescription(scenario, testClass)));
        this.put(feature.getId(), featureDescription);
        return featureDescription;
    }

    private Description createJUnitDescription(final Scenario scenario, final Class<?> testClass) {
        final Description scenarioDescription = Description.createSuiteDescription(scenario.getDescription());
        scenario.getSteps().forEach(step -> scenarioDescription.addChild(createJUnitDescription(step, testClass)));
        this.put(scenario.getId(), scenarioDescription);
        return scenarioDescription;
    }

    private Description createJUnitDescription(final Step step, final Class<?> testClass) {
        final Description stepDescription = Description.createTestDescription(testClass, step.getDescription());
        this.put(step.getId(), stepDescription);
        return stepDescription;
    }

}
