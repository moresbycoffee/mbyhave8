/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.junit;

import java.util.Map;

import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import org.moresbycoffee.mbyhave8.hooks.MByHave8Hooks;
import org.moresbycoffee.mbyhave8.result.FeatureResult;
import org.moresbycoffee.mbyhave8.result.ScenarioResult;
import org.moresbycoffee.mbyhave8.result.SpecOutput;
import org.moresbycoffee.mbyhave8.result.StepResult;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;
import org.moresbycoffee.mbyhave8.structure.Specification;
import org.moresbycoffee.mbyhave8.structure.Step;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

/**
 * TODO javadoc.
 */
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class JUnitNotifier implements MByHave8Hooks {

    private final RunNotifier notifier;
    private final Map<String, Description> descriptionMap;

    @Override
    public void startSpecification(final Specification specification) {
        notifier.fireTestStarted(descriptionMap.get(specification.getId()));
    }

    @Override
    public void endSpecification(final Specification specification, final SpecOutput output) {
        final Description description = descriptionMap.get(specification.getId());
        switch (output.getResult()) {
            case Success: {
                notifier.fireTestFinished(description);
                break;
            }
            case Pending: {
                //TODO decision must be made
                notifier.fireTestIgnored(description);
                break;
            }
            case Failure: {
                notifier.fireTestFailure(new Failure(description, new RuntimeException("TODO")));
                break;
            }
            case Error: {
                notifier.fireTestFailure(new Failure(description, new RuntimeException("TODO")));
                break;
            }
        }
    }

    @Override
    public void startFeature(final Feature feature) {

    }

    @Override
    public void endFeature(final Feature feature, final FeatureResult result) {

    }

    @Override
    public void startScenario(final String featureId, final Scenario scenario) {
        notifier.fireTestStarted(descriptionMap.get(scenario.getId()));
    }

    @Override
    public void endScenario(final String featureId, final Scenario scenario, final ScenarioResult result) {

        final Description description = descriptionMap.get(scenario.getId());
        switch (result) {
            case Success: {
                notifier.fireTestFinished(description);
                break;
            }
            case Failure: {
                notifier.fireTestFailure(new Failure(description, new RuntimeException("TODO")));
                break;
            }
            case Error: {
                notifier.fireTestFailure(new Failure(description, new RuntimeException("TODO")));
                break;
            }
            case Pending: {
                //TODO define how to report this
                notifier.fireTestIgnored(description);
                break;
            }
        }
    }

    @Override
    public void startStep(final String featureId, final String scenarioId, final Step step) {
        notifier.fireTestStarted(descriptionMap.get(step.getId()));
    }

    @Override
    public void endStep(final String featureId, final String scenarioId, final Step step, final StepResult result) {

        final Description description = descriptionMap.get(step.getId());

        if (result instanceof StepResult.Error) {
            notifier.fireTestFailure(new Failure(description, result.getException()));
        } else if (result instanceof StepResult.Failure) {
            notifier.fireTestFailure(new Failure(description, result.getException()));
        } else if (result instanceof StepResult.Pending) {
            //TODO decision must be made
            notifier.fireTestIgnored(description);
        } else if (result instanceof StepResult.Skipped) {
            notifier.fireTestIgnored(description);
        } else if (result instanceof StepResult.Success) {
            notifier.fireTestFinished(description);
        } else {
            throw new IllegalStateException("The [" + result+ "] step result is not supported by the Scenario");
        }
    }
}
