/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.filter;

import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Represents and element in the {@link Filter} chain.
 * An element can be either an {@link Inclusion} or an
 * {@link Exclusion}. Inclusion include elements (and exclude
 * all others), Exclusion excludes elements and do not effect others.
 */
public interface FilterItem {

    public enum FilterResult {
        Undetermined,
        Excluded,
        Included;

        public static FilterResult fold(final FilterResult a, final FilterResult b) {
            switch (a) {
            case Undetermined:
                return b;
            case Included:
                if (FilterResult.Undetermined.equals(b)) {
                    return a;
                } else {
                    return b;
                }
            case Excluded:
                return a;
            default:
                throw new IllegalStateException("No such case");
            }
        }
    }

    FilterResult evaluate(final List<String> tags);


    public static FilterItem parse(final String filterTag) {
        if (filterTag.startsWith("~")) {
            return new Exclusion(filterTag);
        } else {
            return new Inclusion(filterTag);
        }
    }

    /**
     * One of the two {@link FilterItem}s. This represents
     * and exclusion. The features/scenarios with a tag
     * matching to {@link #filterTag} will be excluded from
     * the test.
     */
    @ToString
    @EqualsAndHashCode
    public static class Exclusion implements FilterItem {

        private final String filterTag;

        public Exclusion(final String filterTag) {
            this.filterTag = filterTag.substring(1);
        }

        @Override
        public FilterResult evaluate(final List<String> tags) {
            if (tags != null && !tags.isEmpty()) {
                for (final String tag : tags) {
                    if (filterTag.equals(tag)) {
                        return FilterResult.Excluded;
                    }
                }
            }
            return FilterResult.Included;
        }

    }

    /**
     * One of the two {@link FilterItem}s. This represents
     * and inclusion. The features/scenarios with a tag
     * matching to {@link #filterTag} will be included into
     * the test. All other will be excluded.
     */
    @RequiredArgsConstructor
    @ToString
    @EqualsAndHashCode
    public static class Inclusion implements FilterItem {
        private final String filterTag;

        @Override
        public FilterResult evaluate(final List<String> tags) {
            if (tags != null && !tags.isEmpty()) {
                for (final String tag : tags) {
                    if (filterTag.equals(tag)) {
                        return FilterResult.Included;
                    }
                }
                return FilterResult.Undetermined;
            }
            return FilterResult.Excluded;
        }
    }
}