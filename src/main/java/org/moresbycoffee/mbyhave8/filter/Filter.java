/*
 * Moresby Coffee Bean
 *
 * Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Moresby Coffee nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.moresbycoffee.mbyhave8.filter;

import static java.util.Arrays.*;

import java.util.List;

import org.moresbycoffee.mbyhave8.filter.FilterItem.FilterResult;
import org.moresbycoffee.mbyhave8.structure.Feature;
import org.moresbycoffee.mbyhave8.structure.Scenario;

import lombok.Getter;

/**
 * Filter object what controls what {@link Feature} or {@link Scenario} is executed.
 */
@Getter
public class Filter {

    public static final Filter EMPTY_FILTER = new Filter(null);

    private final FilterItem[] filterTags;

    /**
     * @param initString Coma separated list of filters. (Nullable)
     */
    public Filter(final String initString) {
        if (initString == null || "".equals(initString.trim())) {
            filterTags = new FilterItem[] {};
        } else {
            filterTags = stream(initString.split(",")).map(String::trim).map(FilterItem::parse).toArray(FilterItem[]::new);
        }
    }

    public boolean isExecutable(final List<String> tags) {
        if (filterTags.length == 0) {
            return true;
        }
        final FilterResult result = stream(filterTags).map(item -> item.evaluate(tags)).reduce(FilterResult.Undetermined, FilterResult::fold);

        return FilterResult.Included.equals(result);

    }






}
