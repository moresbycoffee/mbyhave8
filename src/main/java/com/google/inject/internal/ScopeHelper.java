/*
* Moresby Coffee Bean
*
* Copyright (c) 2014, Barnabas Sudy (barnabas.sudy@gmail.com)
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the Moresby Coffee nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL BARNABAS SUDY OR MORESBYCOFFEE BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.google.inject.internal;

import com.google.inject.Key;
import com.google.inject.Provider;

import java.lang.reflect.Field;

/**
* Helper utility class making possible to access internal GuiceInjection structures via reflection.
*/
public class ScopeHelper {

    public static <T> Provider<T> getWorkingProvider(final Provider<T> provider, final Key<T> key) throws NoSuchFieldException, IllegalAccessException, ErrorsException {
        final Field injectorField = ProviderToInternalFactoryAdapter.class.getDeclaredField("injector");
        injectorField.setAccessible(true);
        final InjectorImpl injector = (InjectorImpl) injectorField.get(provider);
        return injector.getProviderOrThrow(key, new Errors());
    }

    /** Hidden constructor of utility class. */
    private ScopeHelper() { /* NOP */ }
}
