# MByHave8

Easy to use, pure Java BDD tool for Java 8.

```
#!java
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.util.*;

import org.moresbycoffee.mbyhave8.MySpec;

public class MySpec extends MByHaveSpec {
    {
        Feature("that proves MByHave8 is a great tool",
            Scenario("my first MByHave8 scenario",
                given("a list with two elements",                 this::initAListWithTwoElements),
                when("a new element is added",                    this::addThirdElement),
                then("the list should consist of three elements", this::assertThreeElements)
            )
        );
    }

    private List<String> list;
    public void initAListWithTwoElements() {
        list = new ArrayList<String>();
        list.add("First");
        list.add("Second");
    }

    public void addThirdElement() {
        list.add("Third");
    }

    public void assertThreeElements() {
        assertThat(list, contains("First", "Second", "Third"));
    }

}

```

For more details visit [Wiki pages](https://bitbucket.org/moresbycoffee/mbyhave8/wiki/).

[![Build Status](https://drone.io/bitbucket.org/moresbycoffee/mbyhave8/status.png)](https://drone.io/bitbucket.org/moresbycoffee/mbyhave8/latest)